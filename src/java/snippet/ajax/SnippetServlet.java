/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.ajax;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import snippet.Authi.Auth;
import snippet.Authi.DBClass;
import snippet.Authi.Group;
import snippet.lib.Language;
import snippet.lib.Snippet;
import snippet.lib.SnippetUser;
import snippet.lib.Tag;

/**
 *
 * @author Alexander
 */
@WebServlet("/Snippet")
public class SnippetServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!Auth.isLoggedIn(request.getSession())) {
            return;
        }

        String action = request.getParameter("action");
        if (action == null) {
            return;
        }

        AjaxResult result = new AjaxResult();

        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String code = request.getParameter("code");
        String language = request.getParameter("language");
        String ispublic = request.getParameter("ispublic");
        String shareWith = request.getParameter("share_with");
        String[] jtags = request.getParameterValues("tags[]");

        if (action.equals("update")) {
            if (id != null && name != null && code != null && language != null
                    && id.length() > 0 && name.length() > 0 && code.length() > 0 && language.length() > 0) {
                boolean pub = false;
                if (ispublic != null && ispublic.length() > 0) {
                    pub = Boolean.parseBoolean(ispublic);
                }
                result.success = UpdateSnippet(Integer.parseInt(id), name, code,
                        (Language) DBClass.GetByID(Language.class, Integer.parseInt(language)), pub);

            } else {
                result.error = "some fields are empty";
            }
        } else if (action.equals("updateshare")) {
            if (id != null && ispublic != null && id.length() > 0 && ispublic.length() > 0) {
                result.success = UpdateSnippetShare(Integer.parseInt(id), Boolean.getBoolean(ispublic));
            } else {
                result.error = "some fields are empty";
            }
        } else if (action.equals("create")) {
            if (name != null && code != null && language != null
                    && name.length() > 0 && code.length() > 0 && language.length() > 0) {
                boolean pub = false;
                if (ispublic != null && ispublic.length() > 0) {
                    pub = Boolean.parseBoolean(ispublic);
                }
                Group _shareWith = null;
                if (shareWith != null && shareWith.length() > 0) {
                    try {
                        boolean isInGroup = Auth.getUser(request.getSession()).IsInGroup((Group) DBClass.GetByID(Group.class, Integer.parseInt(shareWith)));
                        if (isInGroup) {
                            _shareWith = (Group) DBClass.GetByID(Group.class, Integer.parseInt(shareWith));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                List<Tag> tags = new ArrayList<Tag>();

                //JsonReader jsonReader = Json.createReader(new StringReader(jtags));
                //JsonObject parsed_tags = jsonReader.readObject();
                //JsonArray parsed_tags = jsonReader.readArray();
                // todo: tags;
                if (jtags != null) {
                    for (String t : jtags) {
                        //todo TAGS!
                        //JsonObject job = parsed_tags.getJsonObject(i)
                        Tag tag = Tag.GetByTag(Tag.class, "tag", t);
                        if (!tag.isLoaded()) {
                            tag.SetAttr("tag", t);
                            tag.create();
                        }
                        if (tag.isLoaded()) {
                            tags.add(tag);
                        }
                    }
                }
                SnippetUser u = SnippetUser.Cast(SnippetUser.class, Auth.getUser(request.getSession()));
                result.success = CreateSnippet(u, name, code,
                        (Language) DBClass.GetByID(Language.class, Integer.parseInt(language)), pub, tags, _shareWith);
            } else {
                result.error = "some fields are empty";
            }
        } else if (action.equals("remove")) {
            if (id != null && id.length() > 0) {
                result.success = SnippetUser.Cast(SnippetUser.class, Auth.getUser(request.getSession())).RemoveSnippet(
                        (Snippet) DBClass.GetByID(Snippet.class, Integer.parseInt(id)));
                if (!result.success) {
                    result.error = "You can not delete a Snippet if its forked by another user";
                }
            }
        } else if (action.equals("fork")) {
            result = new AjaxForkResult();
            String _snippet = request.getParameter("snippet");
            if (_snippet != null) {
                SnippetUser user = SnippetUser.Cast(SnippetUser.class, Auth.getUser(request.getSession()));

                Snippet snippet = Snippet.GetByID(Snippet.class, Integer.parseInt(_snippet));
                if (snippet.isLoaded()) {
                    if (UserHasPermissionToViewSnippet(request.getSession(), user, snippet)) {
                        Snippet forkedSnippet = Snippet.Fork(snippet, user);
                        if (forkedSnippet != null) {
                            result.success = forkedSnippet.isLoaded();
                            if (result.success) {
                                ((AjaxForkResult) result).id = forkedSnippet.GetID();
                            } else {
                                result.error = "an error occurred during creation";
                            }
                        }
                    }
                }
            }

        }

        response.setContentType("application/json");
        String str = "{\"success\": " + String.valueOf(result.success) + ", \"error\": \"" + result.error + "\"";
        if (result instanceof AjaxForkResult) {
            str += ", \"id\": " + String.valueOf(((AjaxForkResult) result).id);
        }
        str += "}";
        response.getWriter().write(str);
    }

    boolean UpdateSnippet(int snippetid, String name, String code, Language language, boolean isPublic) {
        Snippet snippet = (Snippet) DBClass.GetByID(Snippet.class, snippetid);
        if (snippet == null) {
            return false;
        }
        snippet.SetAttr("title", name);
        snippet.SetAttr("code", code);
        snippet.SetLanguage(language);
        snippet.SetMeta("ISPUBLIC", isPublic ? "1" : "0");
        snippet.saveMeta();
        return snippet.save();
    }

    boolean UpdateSnippetShare(int snippetid, boolean isPublic) {
        Snippet snippet = (Snippet) DBClass.GetByID(Snippet.class, snippetid);
        if (snippet == null) {
            return false;
        }
        snippet.SetMeta("ISPUBLIC", isPublic ? "1" : "0");
        return snippet.save();
    }

    boolean CreateSnippet(SnippetUser creator, String name, String code, Language language, boolean isPublic, List<Tag> tags, Group shareWith) {
        return Snippet.create_static(name, code, creator, language, isPublic, tags, shareWith) != null;
    }

    public boolean UserHasPermissionToViewSnippet(HttpSession session, SnippetUser user, Snippet snippet) {
        // SHOW IF PUBLIC
        if (snippet.IsPublic()) {
            return true;
        }

        if (snippet.HasGroup()) {
            if (snippet.GetGroup().ContainUser(user)) {
                return true;
            } else {
                return false;
            }
        }
        if (!Auth.isLoggedIn(session)) {
            return false;
        }
        if (Auth.getUser(session).GetID() == Integer.parseInt(snippet.GetAttr("creator"))) {
            return true;
        }
        return false;
    }
}
