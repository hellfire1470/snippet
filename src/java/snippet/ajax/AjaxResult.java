/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.ajax;

/**
 *
 * @author Alexander
 */
class AjaxResult {

    public boolean success;
    public String error;
}

class AjaxForkResult extends AjaxResult {

    public int id;
}
