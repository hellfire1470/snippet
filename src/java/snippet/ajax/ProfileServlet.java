/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.ajax;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import snippet.Authi.Auth;
import snippet.Authi.User;

/**
 *
 * @author Alexander
 */
@WebServlet("/Profile")
public class ProfileServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!Auth.isLoggedIn(request.getSession())) {
            return;
        }
        String action = request.getParameter("action");

        if (action == null || action.trim().length() == 0) {
            return;
        }

        AjaxResult result = new AjaxResult();

        if (action.equals("save")) {
            User user = Auth.getUser(request.getSession());
            String location = request.getParameter("location");
            if (location != null) {
                user.SetMeta("Location", location);
            }
            String employedAt = request.getParameter("employed_at");
            if (employedAt != null) {
                user.SetMeta("Employed_at", employedAt);
            }
            String occupation = request.getParameter("occupation");
            if (occupation != null) {
                user.SetMeta("Occupation", occupation);
            }
            String about = request.getParameter("about");
            if (about != null) {
                user.SetMeta("About", about);
            }
            result.success = user.saveMeta();
        }

        response.setContentType("application/json");
        String str = "{\"success\": " + String.valueOf(result.success) + ", \"error\": \"" + result.error + "\"}";
        response.getWriter().write(str);
    }

}
