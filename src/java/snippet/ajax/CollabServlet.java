/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.ajax;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import snippet.Authi.Auth;
import snippet.lib.Collab;
import snippet.lib.SnippetUser;

/**
 *
 * @author Alexander
 */
@WebServlet("/Collab")
public class CollabServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String collabId = request.getParameter("collab");

        AjaxResult result = new AjaxResult();

        if (action == null) {
            return;
        }

        Collab collab = Collab.GetByID(Collab.class, Integer.parseInt(collabId));

        if (!collab.isLoaded()) {
            result.error = "Unknown collaboration";
            response.setContentType("application/json");
            String str = "{\"success\": " + String.valueOf(result.success) + ", \"error\": \"" + result.error + "\"}";
            response.getWriter().write(str);
            return;
        }

        if (action.equals("leave")) {

            if (collab.ContainUser(Auth.getUser(request.getSession()))) {
                if (collab.IsUserAdmin(SnippetUser.Cast(SnippetUser.class, Auth.getUser(request.getSession())))) {
                    if (collab.GetAdmins().size() > 1) {
                        result.success = collab.RemoveUser(Auth.getUser(request.getSession()));
                    } else {
                        result.error = "You are the last admin of " + collab.GetName() + ", add more admins or delete your collab";
                    }
                } else {
                    result.success = collab.RemoveUser(Auth.getUser(request.getSession()));
                }
            }
        }

        if (action.equals("update")) {
            String target = request.getParameter("target");
            if (target != null && target.trim().length() > 0) {
                if (target.equals("user")) {
                    String _userid = request.getParameter("user");
                    String key = request.getParameter("key");
                    String value = request.getParameter("value");
                    if (key != null && value != null) {
                        try {
                            int userid = Integer.parseInt(_userid);
                            SnippetUser user = SnippetUser.GetByID(SnippetUser.class, userid);
                            result.success = UpdateUserPermission(request, collab, user, key, value);
                        } catch (Exception e) {

                        }
                    }
                }
            }
        }

        response.setContentType("application/json");
        String str = "{\"success\": " + String.valueOf(result.success) + ", \"error\": \"" + result.error + "\"}";
        response.getWriter().write(str);
    }

    private boolean UpdateUserPermission(HttpServletRequest req, Collab collab, SnippetUser user, String key, String value) {
        if (!Auth.isLoggedIn(req.getSession())) {
            return false;
        }
        if (collab.IsUserAdmin(SnippetUser.Cast(SnippetUser.class, Auth.getUser(req.getSession())))) {
            if (collab.ContainUser(user)) {
                if (key.equals("ADMIN") && Auth.getUser(req.getSession()).GetID() == user.GetID()) {
                    return false;
                }
                collab.SetUserMeta(user, key, value.equals("true") ? "1" : "0");
                return true;

            }
        }
        return false;
    }
}
