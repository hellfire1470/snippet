/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import snippet.Authi.Auth;

/**
 *
 * @author Alexander
 */
public class Navigation {
    
    private HttpSession session;
    
    public Navigation(){

    }  
        
    public Navigation(HttpSession session){
        this.session = session;
    }

    public static class NavObject {
        
        public NavObject(){
            
        }  
        
        public NavObject(HttpSession session){
            this.session = session;
        }
        
        private HttpSession session;
        private boolean intern = true;
        private boolean onlyLoggedIn = false;
        private boolean onlyLoggedOut = false;
        private String name = "";
        private String link = "";
        private String classes = "";
        private NavList sub = new NavList();

        public void appendNavigation(NavObject obj) {
            sub.objects.add(obj);
        }

        public String getName() {
            return name;
        }

        public String getLink() {
            return link;
        }

        public String getClasses() {
            return classes;
        }

        public NavList getSublist() {
            return sub;
        }

        public boolean isIntern() {
            return intern;
        }

        public boolean isOnlyLoggedIn() {
            return onlyLoggedIn;
        }

        public boolean isOnlyLoggedOut() {
            return onlyLoggedOut;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public void setClasses(String classes) {
            this.classes = classes;
        }

        public void setIntern(boolean intern) {
            this.intern = intern;
        }

        public void setOnlyLoggedIn(boolean onlyLoggedIn) {
            this.onlyLoggedIn = onlyLoggedIn;
        }

        public void setOnlyLoggedOut(boolean onlyLoggedOut) {
            this.onlyLoggedOut = onlyLoggedOut;
        }

        //public String getHTML() {
        //return getHTML(false);
        //}
        public String getHTML(String location) {
            if ((!Auth.isLoggedIn(session) && isOnlyLoggedIn()) || Auth.isLoggedIn(session) && isOnlyLoggedOut()) {
                return "";
            }
            boolean active = isChildSelected(location, this) || getLink().equals(location);
            String html = "<li class=\"" + getClasses() + (active ? " active" : "") + "\">";
            html += "<a href=\"" + (isIntern() ? "?nav=" : "") + getLink() + "\">" + getName() + "</a>";
            if (sub.getObjects().size() > 0) {
                html += "<ul>";
                html += sub.getHTML(location);
                html += "</ul>";
            }
            html += "</li>";
            return html;
        }

        public boolean isChildSelected(String location, NavObject source) {

            for (NavObject obj : source.sub.getObjects()) {
                if (obj.sub.getObjects().size() > 0) {
                    if (isChildSelected(location, obj)) {
                        return true;
                    }
                }
                if (obj.getLink().equals(location)) {
                    return true;
                }

            }
            return false;
        }
    }

    private static NavList _navigation = new NavList();

    public static class NavList {

        private List<NavObject> objects = new ArrayList<NavObject>();

        public void addObject(NavObject no) {
            objects.add(no);
        }

        public List<NavObject> getObjects() {
            return objects;
        }

        public String getHTML(String location) {
            String html = "";
            for (NavObject obj : objects) {
                html += obj.getHTML(location);
            }
            return html;
        }
    }

    public static String getHTML(String location) {
        String html = "";
        html += "<ul>";
        for (Navigation.NavObject obj : _navigation.objects) {
            html += obj.getHTML(location);
        }
        html += "</ul>";
        return html;
    }

    public static void setNavigation(NavList list) {
        _navigation = list;
    }

    public static void appendNavigation(NavList source, NavObject obj) {
        source.objects.add(obj);
    }
}
