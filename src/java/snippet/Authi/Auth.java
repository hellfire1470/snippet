/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.Authi;

import java.sql.Connection;
import java.util.Date;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import snippet.Helper;
import javax.servlet.http.HttpSession;

/**
 *
 * @author J
 */
public class Auth {

    public static User getUser(HttpSession session) {
        //SessionSettings ss = (SessionSettings) request.getSession().getAttribute("Authentication");
        if (isLoggedIn(session)) {
            User user = ((SessionSettings) session.getAttribute("Authentication")).getUser();
            return user;
        } else {
            return null;
        }
    }

    public static boolean isLoggedIn(HttpSession session) {
        if (session == null) {
            return false;
        }
        SessionSettings ss = (SessionSettings) session.getAttribute("Authentication");
        if (ss == null) {
            return false;
        }
        // Check only once  multiple check not needed

        if (ss.getLoginChecked()) {
            return ss.isLoggedIn();
        }

        ss.setLoginChecked(true); // = true;

        if (!ss.getKeepLoggedIn()) {
            long time = new Date().getTime();
            if (ss.getLastActivity().getTime() + Conf.LOGINTIMEOUT < time) {
                ss.setAsLoggedOut(true);
                //_logout = true;
            } else {
                ss.setLastActivity(new Date(time));
                //ss.setLastActivity(lastActivity);
            }
        }

        if (!ss.isLoggedOut()) {
            checkSessionKey();
        } else {
            logout(session);
        }

        return ss.isLoggedIn();//logged_in;
    }

    private static boolean checkSessionKey() {
        return true;
    }

    /**
     * @param $user User or Username
     * @param $keep_logged_in
     */
    private static void _login(HttpSession session, User user, HttpServletResponse response, Boolean keep_logged_in) {
        //SessionSettings ss = (SessionSettings) request.getSession().getAttribute("Authentication");
        SessionSettings ss = new SessionSettings();
        ss.setUser(user);

        if (keep_logged_in) {
            // Generate new session key
            generateNewSessionKey(session, response, user);
        }

        //session_regenerate_id();
        //request.changeSessionId();
        //session.
        //SessionSettings ss = new SessionSettings();
        //ss.setUser(user);
        //ss.setUser(user);
        ss.setKeepLoggedIn(keep_logged_in);
        ss.setSessionid(session.getId());
        ss.setLastActivity(new Date());
        ss.setAsLoggedIn(true);
        session.setAttribute("Authentication", ss);

        //Logger::log("User ". $user->getEmail() . " has logged in. IP: ".Auth::$ip, LogLevel::SYSTEM, Auth::$session_settings, "Login");
    }

    public static String generateRandomString(int length) {
        String characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int charactersLength = characters.length();
        String randomString = "";
        for (int i = 0; i < length; i++) {
            randomString += characters.toCharArray()[(int) (Math.round(Math.random() * (charactersLength - 1)))];
        }
        return randomString;
    }

    private static void generateNewSessionKey(HttpSession session, HttpServletResponse response, User user) {
        //SessionSettings ss = (SessionSettings) request.getSession().getAttribute("Authentication");
        String _session_rndnum = generateRandomString(10);
        String _sessionkey = generateSessionKey(_session_rndnum, user);
        setSessionKey(response, _sessionkey, _session_rndnum, user);
    }

    private static void setSessionKey(HttpServletResponse response, String sessionkey, String rndnum, User user) {
        //SessionSettings ss = (SessionSettings) request.getSession().getAttribute("Authentication");
        //setcookie(Auth::$session_settings, $sessionkey);

        Cookie cookie = new Cookie("Authentication", sessionkey);
        cookie.setMaxAge((int) Conf.LOGINTIMEOUT);
        response.addCookie(cookie);
        // SAVE session key
        //Connection conn = DBCONN.getConnection();
        user.SetAttr("sessionkey", sessionkey);
        user.SetAttr("sessionkey_rndnum", rndnum);
        if (user.save()) {
            /*
            if(!$connection->query("UPDATE ".TABLE_USER." SET sessionkey = '".$connection->real_escape_string($sessionkey)."', sessionkey_rndnum = '".$connection->real_escape_string($rndnum)."' WHERE username = '".$connection->real_escape_string($user->getUsername())."'")){
                Logger::log("Failed to set sessionkey.", LogLevel::ERROR, Auth::$session_settings, "ERROR");
            }*/
        }
    }

    private static String generateSessionKey(String _session_rndnum, User user) {
        ///SessionSettings ss = (SessionSettings) request.getSession().getAttribute("Authentication");
        String _sessisonkey = user.GetUsername() + user.GetID() + _session_rndnum;
        _sessisonkey = MD5.getMd5(Conf.TOKENPRESALT + _sessisonkey + Conf.TOKENPOSTSALT);
        return _sessisonkey;
    }

    public static String hashPassword(String password) {
        return MD5.getMd5(Conf.PRESALT + password + Conf.POSTSALT);
    }

    public static boolean isValidPassword(String password) {
        return (password.length() >= Conf.PASSLENGTHMIN && password.length() <= Conf.PASSLENGTHMAX);
    }

    public static boolean login(HttpSession session, HttpServletResponse response, String username, String password, Boolean keep_logged_in) {
        //SessionSettings ss = (SessionSettings) request.getSession().getAttribute("Authentication");

        if (isLoggedIn(session)) {
            return ((SessionSettings) session.getAttribute("Authentication")).isLoggedIn();
        }

        //Connection conn = DBCONN.getConnection();
        //String _username = $conn->real_escape_string($username);
        //$_password = $conn->real_escape_string($password);
        //$_password_hashed = Auth::hashPassword($_password);
        User __user = new User(username);
        //System.out.print(__user.GetAttr("password"));
        //System.out.println(__user.isLoaded());
        if (__user.isLoaded() && __user.GetAttr("password").equals(hashPassword(password))) {
            //_login(request, response, username, keep_logged_in);
            _login(session, __user, response, keep_logged_in);
            return true;
        } else {
            return false;
        }

        //return logged_in;
        //return ((SessionSettings) request.getSession().getAttribute("Authentication")).isLoggedIn();
        //return true;
    }

    public static void logout(HttpSession session) {
        logout(session, null);
    }

    public static void logout(HttpSession session, HttpServletResponse response) {
        if (!isLoggedIn(session)) {
            return;
        }
        SessionSettings ss = (SessionSettings) session.getAttribute("Authentication");
        //Logger::log("User " . Auth::$user->getEmail() . " has logged out", LogLevel::SYSTEM, Auth::$session_settings, "Logout");

        // remove SESSION KEY
        //Connection conn = DBCONN.getConnection();
        ss.getUser().SetAttr("sessionkey", "");
        ss.getUser().SetAttr("sessionkey_rndnum", "");
        ss.getUser().save();

        if (response != null) {
            //clear cookie
            Cookie cookie = new Cookie("Authentication", "");
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        }
        //unset($_SESSION[Auth::$session_settings]);
        //request.changeSessionId();
        session.invalidate();
    }

    public static RegistrationResult Register(String username, String password, String email, String firstname, String lastname) {
        Connection conn = DBCONN.getConnection();

        //$_user = $conn->real_escape_string($username);
        if (username.length() < Conf.USERLENGTHMIN) {
            return RegistrationResult.USER_SHORT;
        }
        if (username.length() > Conf.USERLENGTHMAX) {
            return RegistrationResult.USER_LONG;
        }

        if (password.length() < Conf.PASSLENGTHMIN) {
            return RegistrationResult.PASSWORD_SHORT;
        }
        if (password.length() > Conf.PASSLENGTHMAX) {
            return RegistrationResult.PASSWORD_LONG;
        }

        //pass = $conn->real_escape_string(Auth::hashPassword($password));
        String _pass = hashPassword(password);

        //String _email = "";
        //if(Helper.isValidEmailAddress(email))
        if (Conf.FORCE_EMAIL_REGISTRATION && email.isEmpty()) {
            return RegistrationResult.REQUIRED_EMAIL;
        } else if (Conf.FORCE_EMAIL_REGISTRATION) {
            //_email = $conn->real_escape_string(filter_var($email, FILTER_VALIDATE_EMAIL));
            if (email == null || email == "" || !Helper.isValidEmailAddress(email)) {
                return RegistrationResult.INVALID_EMAIL;
            }
        }

        if (Conf.REQUIRE_FIRSTNAME && firstname.isEmpty()) {
            return RegistrationResult.REQUIRED_FIRSTNAME;
        }
        //_firstname = $conn->real_escape_string($firstname);

        if (Conf.REQUIRE_LASTNAME && lastname.isEmpty()) {
            return RegistrationResult.REQUIRED_LASTNAME;
        }
        //_lastname = $conn->real_escape_string($lastname);

        //$sql = "INSERT INTO " + TABLE_USER + "(username, password, email, firstname, lastname) VALUES ('" +_user +"', '".$_pass."', '".$_email."', '".$_firstname."', '".$_lastname."')";
        User user = new User();
        user.SetAttr("username", username);
        user.SetAttr("password", _pass);
        user.SetAttr("email", email);
        user.SetAttr("firstname", firstname);
        user.SetAttr("lastname", lastname);

        return user.create() ? RegistrationResult.SUCCESS : RegistrationResult.UNKNOWN_ERROR;
    }
}
