/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.Authi;

import java.net.InetAddress;
import java.util.Date;

/**
 *
 * @author J
 */
public class SessionSettings {

    private User _user = null;
    private InetAddress _inet = null;
    private Date _lastActivity = null;
    private boolean _keepLoggedIn;
    private String _sessionid = null;
    private boolean _login_checked;
    private boolean _logged_in;
    private boolean _logout;

    public SessionSettings(User _user, InetAddress _inet, Date _lastActivity, boolean _keepLoggedIn, String _sessionid, boolean _login_checked, boolean _logged_in) {
        this._user = _user;
        this._inet = _inet;
        this._lastActivity = _lastActivity;
        this._sessionid = _sessionid;
        this._login_checked = _login_checked;
        this._logged_in = _logged_in;
    }

    public SessionSettings() {
        _keepLoggedIn = false;
    }

    public void setUser(User user) {
        this._user = user;
    }

    public User getUser() {
        return this._user;
    }

    public void setInetAddress(InetAddress inetaddress) {
        this._inet = inetaddress;
    }

    public InetAddress getInetAddress() {
        return this._inet;
    }

    public void setLastActivity(Date lastActivity) {
        this._lastActivity = lastActivity;
    }

    public Date getLastActivity() {
        return this._lastActivity;
    }

    public void setSessionid(String sessionid) {
        this._sessionid = sessionid;
    }

    public String getSessionid() {
        return this._sessionid;
    }

    public void setKeepLoggedIn(boolean keepLoggedIn) {
        this._keepLoggedIn = keepLoggedIn;
    }

    public boolean getKeepLoggedIn() {
        return _keepLoggedIn;
    }

    public boolean getLoginChecked() {
        return _login_checked;
    }

    public void setLoginChecked(boolean login_checked) {
        this._login_checked = login_checked;
    }

    public boolean isLoggedIn() {
        return _logged_in;
    }

    public void setAsLoggedIn(boolean _logged_in) {
        this._logged_in = _logged_in;
    }

    public boolean isLoggedOut() {
        return _logout;
    }

    public void setAsLoggedOut(boolean _logout) {
        this._logout = _logout;
    }
}
