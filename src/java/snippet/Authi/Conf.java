/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.Authi;

/**
 *
 * @author J
 */
public class Conf {
    private Conf(){
    }
    
    public static final long LOGINTIMEOUT = 0;
    public static final String PRESALT = "?a2fk84jCa0$21";
    public static final String POSTSALT = "jGH73#12?1$2Dm";
    public static final int PASSLENGTHMIN = 8;
    public static final int PASSLENGTHMAX = 32;
    public static final int USERLENGTHMIN = 4;
    public static final int USERLENGTHMAX = 32;
    public static final String TOKENPRESALT = "_0jAeRkg´bk05$3kGß4*";
    public static final String TOKENPOSTSALT = "&8dhBgf2_?d2L-";
    public static final String HOME_URL = "http://www.snipp.et/";
    public static final boolean FORCE_EMAIL_REGISTRATION = true;
    public static final boolean REQUIRE_FIRSTNAME = true;
    public static final boolean REQUIRE_LASTNAME = true;
}
