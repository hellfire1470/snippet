/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.Authi;

import java.util.Map;

/**
 *
 * @author Alexander
 */
public interface IDBClass {

    public String GetTableName();

    public String GetTableMeta();

    public String[] GetTableAttr();

    public String GetPrimaryKeyName();

    public String[] GetTableAttrCode();

    public String[] GetTableAttrIgnore();

    public Class GetClass();

}
