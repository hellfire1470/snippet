/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.Authi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alexander
 */
public class DBCONN {

    private static Connection sConnection;
    //private static $allConnections = array();

    private static boolean initialized = false;

    private static final String sURL = "jdbc:mysql://127.0.0.1:3306/snippet";
    private static final String sUsername = "snippet";
    private static final String sPassword = "iae";

    protected static void disconnect() throws SQLException {

        if (sConnection != null && !sConnection.isClosed()) {
            sConnection.close();
        }
    }

    /**
     * Returned eine bestehende MySQL-Verbindung
     *
     * @return (mysqli)Connection
     */
    public static Connection getConnection() {
        return sConnect();
    }

    /**
     * Beendet die Verbindung zum MySQL Server
     */
    static void sDisconnect() {
        try {
            if (!sConnection.isClosed()) {
                sConnection.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBCONN.class.getName()).log(Level.SEVERE, null, ex);
        }

        //foreach( DBCONN::$allConnections as $connection ){
        //	$connection->close();
        //};
        //DBCONN::$allConnections = array();
        sConnection = null;
    }

    /**
     * Sorgt dafür, dass wenn das PHP-Script zuende geladen wurde, die
     * Verbindung geschlossen wird
     */
    private static void initialize() {
        if (!initialized) {
            //register_shutdown_function('DBCONN::sDisconnect');
            initialized = true;
        }
    }

    /**
     * Baut eine statische Verbindung auf und returned Sie
     *
     * @return (mysqli)
     */
    private static Connection sConnect() {
        DBCONN.initialize();
        try {
            if (sConnection == null || sConnection.isClosed()) {
                Class.forName("com.mysql.jdbc.Driver");

                sConnection = DriverManager.getConnection(
                        sURL, sUsername, sPassword);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBCONN.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBCONN.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sConnection;
    }

}
