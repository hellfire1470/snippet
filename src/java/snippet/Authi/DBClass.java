/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.Authi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Arrays;

/**
 *
 * @author Alexander
 */
public abstract class DBClass implements IDBClass {

    private Map<String, String> attr = new HashMap<String, String>();
    private Map<String, String> meta = new HashMap<String, String>();
    private Map<String, String> _modified_attr = new HashMap<String, String>();

    protected boolean isLoaded = false;

    /**
     * Läd einen Datenbankeintrag anhand der ID
     *
     * @param id test
     */
    public void loadByID(int id) {
        loadByTag(GetPrimaryKeyName(), String.valueOf(id));
    }

    public static <T extends DBClass> T GetByTag(Class<T> c, String tag, String value) {
        List<T> t = GetAllByTag(c, tag, value);
        if (t.size() > 0) {
            return t.get(0);
        }
        try {
            return c.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T extends DBClass> T GetByID(Class<T> c, int id) {
        try {
            T cs = (T) c.newInstance();

            List<T> list = GetAllByTag(c, cs.GetPrimaryKeyName(), String.valueOf(id));
            if (list.size() == 0) {
                return cs;
            }
            return list.get(0);

        } catch (InstantiationException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Läd einen Datenbankeintrag anhand eines Tags
     *
     * @param tag
     * @param value
     */
    public void loadByTag(String tag, String value) {
        loadByTags(new String[]{tag, value});
    }

    private static String join(String delemeter, String[] strings) {
        String result = "";
        for (String s : strings) {
            result += (s + delemeter);
        }
        return result.substring(0, result.length() - 2);
    }

    /**
     * Läd einen Datenbankeintrag anhand von Tags
     *
     * @param keys
     */
    public void loadByTags(String[] keys) {

        Connection conn = DBCONN.getConnection();

        try {

            String where_string = "";
            List<String> _keys = Arrays.asList(GetTableAttr());
            List<String> wheres = new ArrayList<String>();
            for (int i = 0; i < keys.length / 2; i += 2) {
                String key = keys[i];
                String value = keys[i + 1];

                if (_keys.contains(key)) {
                    //where_string += " " + key + " = '" + value +"' AND";
                    where_string += " " + key + " = " + "?" + " AND";
                    wheres.add(value);
                }
            }
            String sql = "SELECT " + join(", ", GetTableAttr()) + " FROM " + GetTableName() + " WHERE " + where_string.substring(0, where_string.length() - 3);

            PreparedStatement statement = conn.prepareStatement(sql);

            for (int i = 0; i < wheres.size(); i++) {
                statement.setString(i + 1, wheres.get(i));
            }

            ResultSet resultSet;
            resultSet = statement.executeQuery();

            if (resultSet.next()) {

                for (String t_attr : GetTableAttr()) {
                    attr.put(t_attr, resultSet.getString(t_attr));
                }
                _loadMeta();
                isLoaded = true;
            }

            resultSet.close();
            statement.close();

        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Prüft ob ein Eintrag erfolgreich geladen wurde, bzw existiert
     *
     * @return (boolean)
     */
    public boolean isLoaded() {
        return isLoaded;
    }

    public void LoadMeta() {
        _loadMeta();
    }

    /**
     * Läd Metadaten aus der Datenbank des zugehörigen Objektes
     */
    private void _loadMeta() {
        if (GetTableName() == null || GetTableMeta() == null || GetTableMeta().isEmpty()) {
            return;
        }
        Connection conn = DBCONN.getConnection();

        String sql = "SELECT metakey, metavalue FROM " + GetTableMeta() + " WHERE owner = ?";

        PreparedStatement statement;
        try {
            statement = conn.prepareStatement(sql);
            statement.setString(1, attr.get(GetPrimaryKeyName()));

            ResultSet resultSet;
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                meta.put(resultSet.getString("metakey"), resultSet.getString("metavalue"));
            }
            resultSet.close();
            statement.close();

        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Speichert das geladene Objekt in der Datenbank
     */
    public boolean save() {
        Connection conn = DBCONN.getConnection();

        String updateString = _to_attr_update_string();
        if (updateString.trim().length() == 0) {
            return true;
        }

        String sql = "UPDATE " + GetTableName() + " SET " + updateString + " WHERE " + GetPrimaryKeyName() + " = '" + attr.get(GetPrimaryKeyName()) + "'";

        PreparedStatement statement;
        try {
            statement = conn.prepareStatement(sql);
            boolean rowUpdated = statement.executeUpdate() > 0;
            statement.close();

            if (rowUpdated) {
                _modified_attr.clear();
            }

            return rowUpdated && _save_meta();
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;

    }

    private Map<String, String> _ToParams() {
        Map<String, String> params = new HashMap<String, String>();

        String[] tableAttr = GetTableAttr();
        for (Map.Entry<String, String> set : _modified_attr.entrySet()) {
            String key = set.getKey();
            String value = set.getValue();
            if (snippet.Helper.in_array(key, tableAttr)) {
                params.put(key, value);
            }
        }

        return params;
    }

    /**
     * Returned einen absicherten SQL-String (entfernt z.B. escape-strings)
     *
     * @return (String)
     */
    private String _to_attr_update_string() {
        String update_string = " ";
        // PUT ATTR INTO STRING
        for (Map.Entry<String, String> set : _modified_attr.entrySet()) {
            String key = set.getKey();
            String value = set.getValue();
            if (!snippet.Helper.in_array(key, GetTableAttrIgnore()) && key != null && value != null) {
                update_string += " "
                        + /* TODO: ESCAPE THIS KEY */ key
                        + " = "
                        + (value.length() == 0 ? "" : "'")
                        + /* TODO: ESCAPE THIS */ (value.length() == 0 ? "NULL" : value)
                        + (value.length() == 0 ? "" : "'")
                        + ",";
            }

        }
        return update_string.substring(0, update_string.length() - 1);
    }

    /**
     * Returned einen absicherten SQL-String (entfernt z.B. escape-strings)
     *
     * @param (String)
     * @return (String)
     */
    private String _to_attr_insert_string(boolean asKey) {
        String insert_string = "";
        // PUT META INTO STRING
        for (Map.Entry<String, String> set : attr.entrySet()) {
            String key = set.getKey();
            String value = set.getValue();
            if (key != null && value != null && !key.equals(GetPrimaryKeyName())) {
                insert_string += " "
                        + (asKey == false ? (snippet.Helper.in_array(key, GetTableAttrCode()) || value.length() == 0 ? "" : "'") : "")
                        + /* TODO: ESCAPE THIS */ (asKey == true ? key : (value.length() == 0 ? "NULL" : value))
                        + (asKey == false ? (snippet.Helper.in_array(key, GetTableAttrCode()) || value.length() == 0 ? "" : "'") : "")
                        + ",";
            }
        }
        return insert_string.substring(0, insert_string.length() - 1);
    }

    /**
     * Fügt die Metadaten in einen SQL-Insert
     *
     * @return (String)
     */
    private String _to_meta_insert_string() {
        Connection conn = DBCONN.getConnection();
        String insert_string = "";
        // PUT META INTO STRING
        for (Map.Entry<String, String> set : meta.entrySet()) {
            String key = set.getKey();
            String value = set.getValue();
            insert_string += " ('" +/* TODO: ESCAPE THIS */ (key) + "', '" +/* TODO: ESCAPE THIS */ (value) + "', '" +/* TODO: ESCAPE THIS */ (attr.get(GetPrimaryKeyName())) + "'),";
        }
        return insert_string.substring(0, insert_string.length() - 1);
    }

    /**
     * Fügt einen neuen Eintrag in die Datenbank hinzu
     *
     * @return (Boolean)
     */
    public boolean create() {
        Connection conn = DBCONN.getConnection();

        String sql = "INSERT IGNORE INTO " + GetTableName() + " (" + _to_attr_insert_string(true) + ") VALUES (" + _to_attr_insert_string(false) + ")";

        PreparedStatement statement;
        try {
            statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            boolean rowInserted = statement.executeUpdate() > 0;
            ResultSet keys = statement.getGeneratedKeys();
            int key = 0;
            keys.next();
            key = keys.getInt(1);
            keys.close();

            statement.close();

            attr.put(GetPrimaryKeyName(), String.valueOf(key));

            if (rowInserted && _save_meta()) {

                isLoaded = true;
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    public boolean saveMeta() {
        return _save_meta();
    }

    /**
     * Fügt alle Metaeinträge in die Datenbank ein
     *
     * @return (Boolean)
     */
    private boolean _save_meta() {
        Connection conn = DBCONN.getConnection();

        if (meta.size() == 0) {
            return true;
        }

        String meta_save_string = _to_meta_insert_string();

        String sql = "INSERT INTO " + GetTableMeta() + "(metakey, metavalue, owner) VALUES " + meta_save_string + " ON DUPLICATE KEY UPDATE metavalue = VALUES(metavalue)";

        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     * Entfernt einen Eintrag aus der Datenbank
     *
     * @return (Boolean)
     */
    public boolean delete() {
        Connection conn = DBCONN.getConnection();

        String sql = "DELETE FROM " + GetTableName() + " WHERE " + GetPrimaryKeyName() + " = '" + attr.get(GetPrimaryKeyName()) + "'";
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement(sql);
            return ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * Lädt sämtliche Daten aus der Datenbank (Einschränkung durch Filter
     * möglich!) Filtermöglichkeit: $filter : sql statement. Example: WHERE x =
     * y ORDER BY x DESC LIMIT 0,1
     *
     * @param (String)
     * @return (Array)
     */
    public static <T extends DBClass> List<T> GetAll(Class<T> c, String filter) {
        try {
            T cs = (T) c.newInstance();
            String sql = "SELECT " + join(", ", cs.GetTableAttr()) + " FROM " + cs.GetTableName() + " " + filter;
            return GetBySQL(c, sql);
        } catch (InstantiationException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static <T extends DBClass> List<T> GetAll(Class<T> c) {
        return GetAll(c, "");
    }

    /**
     * Läd Einträge aus der Datenbank die der Query ($sql) entsprechen
     *
     * @param (String)
     * @return (Array)
     */
    private void loadFromSQL(String sql) {
        Connection conn = DBCONN.getConnection();

        Statement statement;
        try {
            statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {

                for (String t_attr : GetTableAttr()) {
                    attr.put(t_attr, resultSet.getString(t_attr));
                }

                _loadMeta();
                isLoaded = true;

            }

            resultSet.close();
            statement.close();

        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Läd alle Einträge dessen Tag einem bestimmten Wert entspricht
     *
     * @param (String)
     * @param (String)
     * @return (Array)
     */
    public static <T extends DBClass> List<T> GetAllByTag(Class<T> c, String tag, String value) {
        try {
            T cs = (T) c.newInstance();

            String sql = "SELECT " + join(", ", cs.GetTableAttr()) + " FROM " + cs.GetTableName() + " WHERE " + tag + " = '" + value + "'";

            return GetBySQL(c, sql);
        } catch (InstantiationException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static <T extends DBClass> List<T> GetBySQL(Class<T> c, String sql) {

        List<T> results = new ArrayList<T>();

        try {
            Connection conn = DBCONN.getConnection();

            Statement statement;
            try {
                statement = conn.createStatement();
                ResultSet resultSet = statement.executeQuery(sql);

                while (resultSet.next()) {

                    T cs = (T) c.newInstance();
                    for (String t_attr : cs.GetTableAttr()) {
                        cs.SetAttr(t_attr, resultSet.getString(t_attr));
                    }

                    cs.LoadMeta();
                    cs.isLoaded = true;

                    results.add(cs);
                }

                resultSet.close();
                statement.close();

            } catch (SQLException ex) {
                Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (InstantiationException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }

        return results;
    }

    /**
     * Gibt den Wert des Primärschlüssels aus
     *
     * @return (String)
     */
    public int GetID() {
        if (!isLoaded()) {
            throw new IllegalAccessError("trying to get ID of unloaded instance ");
        }
        return Integer.parseInt(attr.get(GetPrimaryKeyName()));
    }

    /**
     * Prüft ob spezifische Metadaten vorhanden sind
     *
     * @param (String)$meta
     * @return (boolean)
     */
    public boolean HasMeta(String meta) {
        if (this.meta == null || this.meta.isEmpty()) {
            return false;
        }
        return this.meta.containsKey(meta);
    }

    /**
     * Returned Metadaten, wenn vorhanden
     *
     * @param (String)
     * @return (Mixed)
     */
    public String GetMeta(String meta) {

        if (HasMeta(meta)) {
            return this.meta.get(meta);
        }
        return null;
    }

    public Map<String, String> GetMeta() {
        return this.meta;
    }

    /**
     * Setzt Metadaten
     *
     * @param (mixed)
     * @param (String)
     */
    public void SetMeta(String meta, String value) {
        this.meta.put(meta, value);
    }

    public void SetMeta(Map<String, String> meta) {
        this.meta = meta;
    }

    /**
     * Prüft ob ein Objekt ein bestimmtes Attribut hat
     *
     * @param (String)
     * @return (boolean)
     */
    public boolean HasAttr(String attr) {
        if (this.attr.get(attr) == null) {
            return false;
        }
        return this.attr.get(attr).length() > 0;
    }

    /**
     * Holt ein spezifisches Spezifisches Attribut
     *
     * @param (String)
     * @return (mixed)
     */
    public String GetAttr(String attr) {

        if (HasAttr(attr)) {
            return this.attr.get(attr);
        }
        return null;
    }

    public Map<String, String> GetAttr() {
        return attr;
    }

    /**
     * Setzt ein Attribut im Attribut-Array
     *
     * @param (mixed)
     * @param (String)
     */
    public void SetAttr(String attr, String value) {
        this.attr.put(attr, value);
        _modified_attr.put(attr, value);
    }

    public void SetAttr(Map attr) {
        this.attr = attr;
        _modified_attr = attr;
    }

    /**
     * Konvertiert ein Objekt
     *
     * @param (Object)
     * @return (Object)
     */
    public static <T extends DBClass> T Cast(Class<T> cl, DBClass obj) {
        if (obj.isLoaded()) {
            try {
                T s = (T) cl.newInstance();
                s.SetAttr(obj.GetAttr());
                s.SetMeta(obj.GetMeta());
                s.isLoaded = obj.isLoaded;
                return s;
            } catch (InstantiationException ex) {
                Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;

    }
}
