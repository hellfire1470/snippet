/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.Authi;

/**
 *
 * @author J
 */
public enum RegistrationResult {
    SUCCESS,
    REQUIRED_EMAIL,
    INVALID_EMAIL,
    USER_SHORT,
    USER_LONG,
    USER_EXISTS,
    PASSWORD_SHORT,
    PASSWORD_LONG,
    REQUIRED_FIRSTNAME,
    REQUIRED_LASTNAME,
    MYSQL_ERROR,
    UNKNOWN_ERROR;
}
