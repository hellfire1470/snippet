/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.Authi;

/**
 *
 * @author Alexander
 */
public class UserGroup extends DBClass {

    private boolean userLoaded = false;
    private User _user = null;

    @Override
    public String GetTableName() {
        return "usergroups";
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetTableMeta() {
        return "usergroupsmeta";
    }

    @Override
    public String[] GetTableAttr() {
        return new String[]{"usergroupid", "groupid", "userid"};
    }

    @Override
    public String GetPrimaryKeyName() {
        return "usergroupid";
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrCode() {
        return null;
    }

    @Override
    public String[] GetTableAttrIgnore() {
        return null;
    }

    @Override
    public Class GetClass() {
        return UserGroup.class;
    }

    public User GetUser() {
        if (!userLoaded) {
            User user = User.GetByID(User.class, Integer.parseInt(GetAttr("userid")));
            if (user.isLoaded()) {
                _user = user;
                userLoaded = true;
            }
        }
        return _user;
    }
}
