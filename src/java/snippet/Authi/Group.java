/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.Authi;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alexander
 */
public class Group extends DBClass {

    @Override
    public String GetTableName() {
        return "groups"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetTableMeta() {
        return "groupsmeta"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttr() {
        return new String[]{"groupid", "name", "description"}; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetPrimaryKeyName() {
        return "groupid"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrCode() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrIgnore() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class GetClass() {
        return Group.class; //To change body of generated methods, choose Tools | Templates.
    }

    public List<User> GetUsers() {
        List<UserGroup> usergroups = _getAllUsergroups();
        List<User> users = new ArrayList<User>();

        for (UserGroup ug : usergroups) {
            User user = (User) User.GetByID(User.class, Integer.parseInt(ug.GetAttr("userid")));
            if (user != null) {
                users.add(user);
            }
        }

        return users;
    }

    public boolean AddUser(User user) {

        UserGroup usergroup = new UserGroup();
        usergroup.SetAttr("groupid", String.valueOf(GetID()));
        usergroup.SetAttr("userid", String.valueOf(user.GetID()));

        return usergroup.create();
    }

    public boolean ContainUser(User user) {
        for (User _user : GetUsers()) {
            if (_user.GetID() == user.GetID()) {
                return true;
            }
        }
        return false;
    }

    public boolean RemoveUser(User user) {

        try {
            Connection conn = DBCONN.getConnection();

            String sql = "DELETE FROM usergroups WHERE userID = '" + user.GetID() + "' AND groupID = '" + GetID() + "'";

            return conn.prepareStatement(sql).execute();
        } catch (SQLException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private UserGroup _GetUsergroup(User user) {
        List<UserGroup> dclass = UserGroup.GetAll(UserGroup.class, "where userid = " + user.GetID() + " AND groupid = " + GetID());
        if (dclass.isEmpty()) {
            return null;
        }
        UserGroup usergroup = (UserGroup) dclass.get(0);
        return usergroup;
    }

    public boolean SetUserMeta(User user, String key, String value) {
        UserGroup ug = _GetUsergroup(user);
        if (ug == null) {
            return false;
        }

        ug.SetMeta(key, value);
        return ug.save();
    }

    public String GetUserMeta(User user, String meta) {
        UserGroup ug = _GetUsergroup(user);
        if (ug == null) {
            return "";
        }
        return ug.GetMeta(meta);
    }

    public boolean HasUserMeta(User user, String meta) {
        UserGroup ug = _GetUsergroup(user);
        if (ug == null) {
            return false;
        }
        return ug.HasMeta(meta);
    }

    public List<User> GetAllUsersByMeta(String meta) {
        return GetAllUsersWithMeta(meta, null);
    }

    public List<User> GetAllUsersWithMeta(String meta, String value) {
        List<UserGroup> usergroups = _getAllUsergroups();
        List<User> users = new ArrayList();

        for (UserGroup ug : usergroups) {
            if (ug.GetMeta(meta).equals(value)) {
                users.add(ug.GetUser());
            }
        }

        return users;
    }

    private List<UserGroup> _getAllUsergroups() {
        List<UserGroup> ug = UserGroup.GetAllByTag(UserGroup.class, "groupid", String.valueOf(GetID()));
        return ug;
    }

    public String GetName() {
        return GetAttr("name");
    }

    public String GetDescription() {
        return GetAttr("description");
    }
}
