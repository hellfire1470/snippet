/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.Authi;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alexander
 */
public class User extends DBClass {

    private List<Group> groups = new ArrayList<Group>();

    public User() {
    }

    public User(String username) {
        super();
        if (username != null && username.length() > 0) {
            loadByTag("username", username);
        }
    }

    public String GetEmail() {
        return GetAttr("email");
    }

    public String GetUsername() {
        return GetAttr("username");
    }

    public String GetFirstName() {
        return GetAttr("firstname");
    }

    public String GetLastName() {
        return GetAttr("lastname");
    }

    public List<Group> GetGroups() {

        List<UserGroup> ugs = UserGroup.GetAllByTag(UserGroup.class, "userid", String.valueOf(GetID()));
        List<Group> groups = new ArrayList<Group>();

        for (UserGroup ug : ugs) {
            Group group = (Group) DBClass.GetByID(Group.class, Integer.parseInt(ug.GetAttr("groupid")));
            if (group != null) {
                groups.add(group);
            }
        }

        this.groups = groups;

        return this.groups;
    }

    public boolean IsInGroup(Group group) {
        int gid = group.GetID();
        return IsInGroup(gid);
    }

    public boolean IsInGroup(int gid) {
        for (Group _group : GetGroups()) {
            if (_group.GetID() == gid) {
                return true;
            }
        }
        return false;
    }

    /*
    public static boolean register(String username, String password, String email, String firstname, String lastname) {
        return Auth.register(username, password, email, firstname, lastname);
    }

    public function changePassword($newPassword) {
        if (!Auth::isValidPassword



            ($newPassword)




            ){
			return false;
        }
        $conn = DBCONN::getConnection
        ();
		$sql = "UPDATE ".static::$table_main. " SET password = '".$conn
        ->real_escape_string(Auth::hashPassword($newPassword))."' WHERE userID = '".$conn -> real_escape_string($this -> getID())."'";

        return $conn -> query($sql) != false;
    }

    public function checkPassword($password) {
        $conn = DBCONN::getConnection
        ();
		$sql = "SELECT userID FROM ".static::$table_main. " WHERE password = '".$conn
        ->real_escape_string(Auth::hashPassword($password))."' AND userID = '".$conn -> real_escape_string($this -> getID())."'";
        if ($result = $conn -> query($sql)) {
            $success = $result -> num_rows == 1;
            $result -> close();
            return $success;
        }
        return false;
    }
     */
    @Override
    public String GetTableName() {
        return "user"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetTableMeta() {
        return "usermeta"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttr() {
        return new String[]{
            "username", "email", "firstname", "lastname", "userID", "password", "sessionkey", "sessionkey_rndnum"
        };
    }

    @Override
    public String GetPrimaryKeyName() {
        return "userID"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrCode() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrIgnore() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class GetClass() {
        return User.class; //To change body of generated methods, choose Tools | Templates.
    }
}
