/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.lib;

import java.util.ArrayList;
import java.util.List;
import snippet.Authi.*;

/**
 *
 * @author Alexander
 */
public class Snippet extends DBClass {

    private SnippetUser _creator;
    private Language _language;
    private Boolean _gotLanguage = false;
    private Group _group = null;
    private boolean _group_loaded = false;

    private SnippetUser _forkedby = null;
    private boolean _forkedby_loaded = false;

    private List<Tag> _tags = new ArrayList<Tag>();

    private boolean _tags_loaded = false;

    public Snippet() {
    }

    public SnippetUser GetCreator() {
        return _creator;
    }

    /**
     * Lädt ein Snippet anhand der ID
     *
     * @param (String)
     * @return (Object)
     */
    @Override
    public void loadByID(int id) {
        super.loadByID(id);
        String creator = GetAttr("creator");
        if (creator != null) {
            _creator = (SnippetUser) DBClass.GetByID(SnippetUser.class, Integer.parseInt(creator));
        }
        String language = GetAttr("languageid");
        if (language != null) {
            _language = (Language) DBClass.GetByID(Language.class, Integer.parseInt(language));
        }
    }

    /**
     * Gibt alle zu einem Snippet gehörigen Tags zurück
     *
     * @return (Array)
     */
    public List<Tag> GetTags() {
        return _loadTags();
    }

    /**
     * Läd alle zu einem Snippet gehörigen Tags aus der Datenbank
     *
     * @return (Array)
     */
    private List<Tag> _loadTags() {
        if (_tags_loaded) {
            return _tags;
        }
        _tags_loaded = true;

        List<Tag> tags = new ArrayList<Tag>();

        List<SnippetTag> snippettags = SnippetTag.GetAll(SnippetTag.class, "WHERE snippetid = '" + String.valueOf(GetID()) + "'");
        for (SnippetTag st : snippettags) {
            Tag tag = Tag.GetByID(Tag.class, Integer.parseInt(st.GetAttr("tagid")));
            tags.add(tag);
        }

        return tags;
    }

    /**
     * Gibt die Programmiersprache eines Snippets zurück
     *
     * @return (Array)
     */
    public Language GetLanguage() {
        if (!_gotLanguage) {
            loadLanguage();
        }
        return _language;
    }

    private void loadLanguage() {
        _language = Language.GetByID(Language.class, Integer.parseInt(GetAttr("languageid")));
        _gotLanguage = true;
    }

    /**
     * Setzt die Programmiersprache eines Snippets
     *
     * @param (String)
     */
    public void SetLanguage(Language language) {
        SetAttr("languageid", String.valueOf(language.GetID()));
        save();
    }

    /**
     * Erstellt ein Snippet
     *
     * @param (String)
     * @param (String)
     * @param (String)
     * @param (String)
     * @param (String)
     * @param (String)
     * @param (String)
     * @return (Boolean)
     */
    public static Snippet create_static(String title, String code, SnippetUser creator, Language language, boolean isPublic, List<Tag> tags, Group share_with) {

        Snippet snippet = new Snippet();
        snippet.SetAttr("title", title);
        snippet.SetAttr("code", code);
        snippet.SetAttr("languageid", String.valueOf(language.GetID()));
        snippet.SetAttr("creator", String.valueOf(creator.GetID()));
        snippet.SetAttr("owner", String.valueOf(creator.GetID()));
        snippet.SetAttr("created", "NOW()");
        if (share_with != null) {
            snippet.SetAttr("groupid", String.valueOf(share_with.GetID()));
        }
        snippet.SetMeta("ISPUBLIC", isPublic ? "1" : "0");

        if (snippet.create()) {
            snippet.AddTags(tags);
            return snippet;
        }
        return null;
    }

    /**
     * Gibt die Gruppe eines Snippets zurück
     *
     * @return (Object)
     */
    public Group GetGroup() {
        if (_group_loaded) {
            return _group;
        }
        if (HasGroup()) {
            Group group = (Group) DBClass.GetByID(Group.class, Integer.parseInt(GetAttr("groupid")));
            if (group != null) {
                _group = group;
                _group_loaded = true;
                return group;
            }
        }
        return null;
    }

    /**
     * Prüft ob das Snippet eine Gruppe hat
     *
     * @return (Boolean)
     */
    public boolean HasGroup() {
        return HasAttr("groupid");
    }

    /**
     * Prüft ob das Snippet geforked
     *
     * @return (Boolean)
     */
    public boolean IsForked() {
        return HasAttr("forkedby");
    }

    /**
     * Gibt den User zurück, der das Snippet geforked hat
     *
     * @return (Object)
     */
    public SnippetUser GetForker() {
        if (_forkedby_loaded) {
            return _forkedby;
        }
        if (IsForked()) {

            SnippetUser user = (SnippetUser) DBClass.GetByID(SnippetUser.class, Integer.parseInt(GetAttr("forkedby")));

            if (user != null) {
                _forkedby = user;
                _forkedby_loaded = true;
                return _forkedby;
            }
        }
        return null;
    }

    /**
     * Gibt das Originalsnippet des geforkten zurück
     *
     * @return (Object)
     */
    public Snippet GetParentSnippet() {
        if (IsForked()) {
            return (Snippet) DBClass.GetByID(Snippet.class, Integer.parseInt(GetAttr("forkedfrom")));
        }
        return null;
    }

    /**
     * Erstellt ein geforktes Snippet
     *
     * @param (Object)
     * @param (Object)
     */
    public static Snippet Fork(Snippet snippet, SnippetUser forker) {
        String forkerid = String.valueOf(forker.GetID());

        if (snippet.isLoaded()) {
            Snippet fSnippet = new Snippet();
            fSnippet.SetAttr(snippet.GetAttr());
            fSnippet.SetAttr("forkedby", snippet.GetAttr("creator"));
            fSnippet.SetAttr("creator", forkerid);
            fSnippet.SetAttr("owner", forkerid);
            fSnippet.SetAttr("created", "NOW()");
            fSnippet.SetAttr("forkedfrom", String.valueOf(snippet.GetID()));
            fSnippet.SetAttr("lastedit", "NULL");
            if (fSnippet.create()) {
                fSnippet.AddTags(snippet.GetTags());
                return fSnippet;
            }
        }
        return null;
    }

    /**
     * Löscht ein Snippet
     *
     * @return (Boolean)
     */
    @Override
    public boolean delete() {

        List<Snippet> cSnippets = GetAllChildrenSnippets();
        if (cSnippets.size() > 0) {
            if (IsForked()) {
                for (Snippet snippet : cSnippets) {
                    snippet.SetAttr("forkedfrom", GetAttr("forkedfrom"));
                    snippet.save();
                }
            } else {
                for (Snippet snippet : cSnippets) {
                    snippet.SetAttr("forkedfrom", null);
                    snippet.SetAttr("forkedby", null);
                    snippet.save();
                }
            }
        }
        return super.delete();
    }

    /**
     * Liefert alle geforkten Snippets eines originalen Snippets zurück
     *
     * @return (Array)
     */
    public List<Snippet> GetAllChildrenSnippets() {
        List<Snippet> snippets = Snippet.GetAll(Snippet.class, "WHERE forkedfrom = '" + String.valueOf(GetID()) + "'");
        return snippets;
    }

    /**
     * Fügt ein neues Tag hinzu
     *
     * @param (Array)
     */
    public void AddTags(List<Tag> tags) {
        for (Tag tag : tags) {
            if (tag.isLoaded()) {
                SnippetTag snippettag = new SnippetTag();
                snippettag.SetAttr("snippetid", String.valueOf(GetID()));
                snippettag.SetAttr("tagid", String.valueOf(tag.GetID()));
                snippettag.create();
            }
        }
    }

    /**
     * Prüft ob ein Snippet öffentlich ist
     */
    public boolean IsPublic() {
        if (HasMeta("ISPUBLIC")) {
            return Integer.parseInt(GetMeta("ISPUBLIC")) == 1;
        }
        return false;
    }

    @Override
    public String GetTableName() {
        return "snippet"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetTableMeta() {
        return "snippetmeta"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttr() {
        return new String[]{"snippetid", "code", "created", "owner", "creator", "languageid", "title", "lastedit", "groupid", "forkedby", "forkedfrom"}; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetPrimaryKeyName() {
        return "snippetid"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrCode() {
        return new String[]{"created", "groupid", "lastedit", "creator", "forkedfrom", "forkedby"}; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrIgnore() {
        return new String[]{"lastedit"}; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class
            GetClass() {
        return Snippet.class; //To change body of generated methods, choose Tools | Templates.
    }
}
