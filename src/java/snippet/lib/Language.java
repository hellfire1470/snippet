/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.lib;

import java.util.ArrayList;
import java.util.List;
import snippet.Authi.*;

/**
 *
 * @author Alexander
 */
public class Language extends DBClass {

    public Language() {
    }

    @Override
    public String GetTableName() {
        return "language"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetTableMeta() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttr() {
        return new String[]{"language", "languageid"}; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetPrimaryKeyName() {
        return "languageid"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrCode() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrIgnore() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class GetClass() {
        return Language.class; //To change body of generated methods, choose Tools | Templates.
    }

    public String GetName() {
        return GetAttr("language");
    }

    public static List<Language> GetLanguages() {
        List<Language> languages = Language.GetAll(Language.class, "");
        return languages;
    }
}
