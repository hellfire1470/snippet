/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.lib;

import java.util.ArrayList;
import java.util.List;
import snippet.Authi.*;

/**
 *
 * @author Alexander
 */
public class Collab extends Group {

    private List<Snippet> _snippets = new ArrayList<Snippet>();
    private boolean _snippets_loaded = false;

    private List<SnippetUser> _admins = new ArrayList<SnippetUser>();
    private boolean _admins_loaded = false;

    /**
     * Holt alle User die Admin sind aus der Datenbank und returned diese
     *
     * @return (Array)
     */
    public List<SnippetUser> GetAdmins() {

        if (!_admins_loaded) {
            _admins_loaded = true;
            List<User> admins = GetAllUsersWithMeta("ADMIN", "1");
            List<SnippetUser> snippadmins = new ArrayList<SnippetUser>();
            for (User admin : admins) {
                snippadmins.add(SnippetUser.Cast(SnippetUser.class, admin));
            }
            _admins = snippadmins;
        }
        return _admins;
    }

    /**
     * Fügt einen neuen Administrator hinzu
     *
     * @param user
     * @return (boolean)
     */
    public boolean AddAdmin(SnippetUser user) {
        return SetUserMeta(user, "ADMIN", "1");
    }

    /**
     * Läd alle Snippets der Collaboration und returned Sie
     *
     * @return (Array)
     */
    public List<Snippet> GetSnippets() {
        if (_snippets_loaded) {
            return _snippets;
        }
        List<Snippet> snippets = Snippet.GetAll(Snippet.class, "WHERE snippet.groupid = '" + String.valueOf(GetID()) + "' ORDER BY snippet.created DESC");

        _snippets = snippets;
        _snippets_loaded = true;
        return snippets;
    }

    /**
     * Prüft ob ein User ein Admin ist
     *
     * @param (Object)
     * @return (Boolean)
     */
    public boolean IsUserAdmin(SnippetUser user) {

        for (SnippetUser cAdmin : GetAdmins()) {
            if (user.GetID() == cAdmin.GetID()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Prüft ob ein User die Rechte dazu hat die Collab-Snippets zu betrachten
     *
     * @param (Object)
     * @return (Boolean)
     */
    public boolean CanUserViewSnippets(SnippetUser user) {
        return GetUserMeta(user, "VIEWSNIPPETS").equals("1");
    }

    /**
     * Prüft ob ein User die Rechte dazu hat die Mitgliederliste eines Collabs
     * anzusehen
     *
     * @param (Object)
     * @return (Boolean)
     */
    public boolean CanUserViewUsers(SnippetUser user) {
        return GetUserMeta(user, "VIEWUSERS").equals("1");
    }

    /**
     * Prüft ob eine Collab öffentlich oder privat ist und ob man joinen kann
     *
     * @return (Boolean)
     */
    public boolean IsPublic() {
        return GetMeta("ISPUBLIC").equals("1");
    }

    /**
     * Prüft ob eine Collab öffentlich sichtbar ist
     *
     * @return (Boolean)
     */
    public boolean IsPubliclyListed() {
        return GetMeta("ISPUBLICLYLISTED").equals("1");
    }

    /**
     * Läd alle öffentlich zugänglichen Collaborations
     *
     * @param (String)
     * @return (Array)
     */
    public static List<Collab> GetAllPubliclyListed(String filter) {

        List<Collab> collabs = Collab.GetAll(Collab.class, "JOIN groupsmeta ON groupsmeta.owner = groups.groupid WHERE groupsmeta.metakey = 'ISPUBLICLYLISTED' AND groupsmeta.metavalue = '1' " + filter);
        return collabs;
    }

    /**
     * Fügt einen User zu einer Collaboration hinzu
     *
     * @param user baa
     * @return successfully added
     */
    @Override
    public boolean AddUser(User user) {
        if (super.AddUser(user)) {
            SetUserMeta(user, "VIEWSNIPPETS", "1");
            SetUserMeta(user, "VIEWUSERS", "1");
            SetUserMeta(user, "WRITESNIPPETS", "1");
            return true;
        }

        return false;
    }
}
