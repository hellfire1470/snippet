/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.lib;

import java.util.ArrayList;
import java.util.List;
import snippet.Authi.*;

/**
 *
 * @author Alexander
 */
public class SnippetUser extends User {

    private List<Snippet> _snippets = new ArrayList<Snippet>();
    private boolean _loadSnippetsDone = false;

    /**
     * Konstruktor: Lädt einen User mit Username aus der Datenbank und speichert
     * ihn in der Klasse
     *
     * @param (String)
     */
    public SnippetUser() {
    }

    public SnippetUser(String username) {
        super(username);
    }

    /**
     * Läd alle zum User gehörenden Snippets aus der Datenbank und returned sie
     *
     * @return (Array)Snippet
     */
    public List<Snippet> GetSnippets() {
        if (!_loadSnippetsDone) {
            _LoadSnippets();
        }
        return _snippets;
    }

    /**
     * Läd alle Snippets aus der Datenbank und speichert Sie in einem Array
     */
    private void _LoadSnippets() {
        if (_loadSnippetsDone) {
            return;
        }
        _loadSnippetsDone = true;

        List<Snippet> snippets = Snippet.GetAll(Snippet.class, "WHERE snippet.owner = '" + String.valueOf(GetID()) + "' ORDER BY created DESC");

        _snippets = snippets;
    }

    /**
     * Entfernt ein Snippet mit einer spezifischen SnippetID aus der Datenbank
     *
     * @param (String)
     * @return boolean
     */
    public boolean RemoveSnippet(Snippet snippet) {
        if (HasSnippet(snippet)) {
            snippet.delete();
            return true;
        }
        return false;
    }

    /**
     * Prüft die Zugehörigkeit eines Snippets zu einem User
     *
     * @param (Object)
     */
    public boolean HasSnippet(Snippet snippet) {
        for (Snippet _snippet : GetSnippets()) {
            if (_snippet.GetID() == snippet.GetID()) {
                return true;
            }
        }
        return false;
    }
}
