/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.lib;

import snippet.Authi.*;

/**
 *
 * @author Alexander
 */
public class Tag extends DBClass {

    public Tag() {
    }

    @Override
    public String GetTableName() {
        return "tags"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetTableMeta() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttr() {
        return new String[]{"tag", "tagid"}; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetPrimaryKeyName() {
        return "tagid"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrCode() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrIgnore() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class GetClass() {
        return Tag.class; //To change body of generated methods, choose Tools | Templates.
    }

}
