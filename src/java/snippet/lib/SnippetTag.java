/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snippet.lib;

import snippet.Authi.*;

/**
 *
 * @author Alexander
 */
public class SnippetTag extends DBClass {

    public SnippetTag() {
    }

    @Override
    public String GetTableName() {
        return "snippettag"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetTableMeta() {
        return ""; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttr() {
        return new String[]{"snippettagid", "snippetid", "tagid"}; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetPrimaryKeyName() {
        return "snippettagid"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrCode() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] GetTableAttrIgnore() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class GetClass() {
        return SnippetTag.class; //To change body of generated methods, choose Tools | Templates.
    }

}
