<%--
Document   : profile
Created on : 13.01.2019, 00:38:14
Author     : J
--%>
<%@page import="snippet.Authi.*" %>
<%!
    public boolean changePassword(User user, String current_password, String new_password, String newpassword2) {
        return false;
    }
%>
<%
    //out.print(Auth.getUser(session).GetEmail());
    String username = request.getParameter("user");
    User user;
    if (username != null && !username.isEmpty() || Auth.isLoggedIn(session)) {
        if (username != null && !username.isEmpty()) {
            user = new User(username);
        } else {
            user = (User) Auth.getUser(session);
        }
        if (!user.isLoaded()) {
%>
<div class="profile-area">
    <h1> User not found</h1>
</div>
<%
        return;
    }

    String change_password = request.getParameter("change_password");
    String current_password;
    String new_password;
    String new_password2;
    if (change_password != null) {
        current_password = request.getParameter("current_password");
        new_password = request.getParameter("current_password");
        new_password2 = request.getParameter("current_password");
        if (current_password != null && !current_password.isEmpty() && new_password != null && !new_password.isEmpty() && new_password2 != null && !new_password2.isEmpty()) {
            if (changePassword(user, current_password, new_password, new_password2)) {
                out.print("<script>success('Password changed');</script>");
            } else {
                out.print("<script>error('Failed :(');</script>");
            }
        }
    }
%>
<div class="profile-area">
    <h1>User profile: <% out.print(user.GetFirstName() + " " + user.GetLastName()); %></h1>
    <section class="page-section profile-section">
        <div class="userInfo">
            <h2><% out.print(user.GetFirstName() + " " + user.GetLastName());%></h2>
            <div class="bio left">
                <jsp:include page="profile_bin.jsp">
                    <jsp:param name="userid" value="<%= user.GetID()%>"></jsp:param>
                    <jsp:param name="info" value="About"></jsp:param>
                    <jsp:param name="placeholder" value="Write something about you..."></jsp:param>
                </jsp:include>
            </div>
            <div class="info right">
                <jsp:include page="profile_bin.jsp">
                    <jsp:param name="userid" value="<%= user.GetID()%>"></jsp:param>
                    <jsp:param name="info" value="Location"></jsp:param>
                    <jsp:param name="placeholder" value="Where are you living?"></jsp:param>
                </jsp:include>
                <jsp:include page="profile_bin.jsp">
                    <jsp:param name="userid" value="<%= user.GetID()%>"></jsp:param>
                    <jsp:param name="info" value="Occupation"></jsp:param>
                    <jsp:param name="placeholder" value="What is your job?"></jsp:param>
                </jsp:include>
                <jsp:include page="profile_bin.jsp">
                    <jsp:param name="userid" value="<%= user.GetID()%>"></jsp:param>
                    <jsp:param name="info" value="Employed at"></jsp:param>
                    <jsp:param name="placeholder" value="Where are you working at?"></jsp:param>
                </jsp:include>
            </div>
            <div class="label-editable">Contact</div>
            <div class="value-editable"><a href="mailto:<% out.print(user.GetEmail()); %>"><% out.print(user.GetEmail()); %></a></div>
        </div>
        <div class="save-profile">
            <%
                if (Auth.isLoggedIn(session)) {
                    if (Auth.getUser(session).GetID() == user.GetID()) {
            %>
            <a href="#" class="btn green" id="lnk_edit_profile" onclick="javascript: editProfile(); return false;">Edit</a>
            <%
                    }
                }
            %>
        </div>
    </section>
</div>
<%
    if (Auth.isLoggedIn(session)) {
        if (Auth.getUser(session).GetID() == user.GetID()) {
%>
<h1>Change Password</h1>
<section class="page-section profile-section">
    <div class="account-settings">
        <div class="change-password">
            <form method="post">
                <div class="widget"><input class="text" type="password" name="current_password" placeholder="Current Password"></div>
                <div class="widget"><input class="text" type="password" name="new_password" placeholder="New Password"></div>
                <div class="widget"><input class="text" type="password" name="new_password2" placeholder="Confirm Password"></div>
                <input class="btn green submit" type="submit" name="change-password" value="Change Password">
            </form>
        </div>
    </div>
</section>
<%
        }
    }
%>
<div class="profile-area">
    <h1>Snippets of <%= user.GetFirstName() + " " + user.GetLastName()%></h1>

    <jsp:include page="search.jsp">
        <jsp:param name="s" value="<%= user.GetEmail()%>"></jsp:param>
    </jsp:include>

</div>
<%                    }
%>
<script>
    var editProfile = function () {

        if ($('.editable').attr("contenteditable") == undefined || $('.editable').attr("contenteditable") == 'false') {
            $('.editable').attr("contenteditable", true);
            $('.editable').addClass('editing');
            $('#lnk_edit_profile').text("Save");
        } else {
            saveProfile();
        }
        return false;
    }

    var saving = false;
    var saveProfile = function () {

        $('#lnk_edit_profile').text("Saving ...");
        if (!saving) {
            saving = true;
            $.ajax({
                url: "Profile",
                type: "POST",
                dataType: "json",
                data: {action: "save", location: $('#Location').html(), occupation: $('#Occupation').html(), employed_at: $('#Employed_at').html(), about: $('#About').html()}
            }).done(function (result) {
                if (result.success) {
                    $('.editable').attr("contenteditable", false);
                    $('.editable').removeClass('editing');
                    $('#lnk_edit_profile').text("Success");
                    success('Successfully updated profile information', 2000);
                    saving = false;
                } else {
                    $('#lnk_edit_profile').text("Failed");
                    error("Failed to update profile information", 5000);
                    saving = false;
                }
            }).error((err) => {
                console.log(err);
            });
        }
    }
</script>