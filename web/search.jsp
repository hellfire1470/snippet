<%--
    Document   : search
    Created on : 16.02.2019, 22:43:29
    Author     : Alexander
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="snippet.Authi.DBCONN"%>
<%@page import="java.util.Date"%>
<link rel="stylesheet" href="js/codemirror/lib/codemirror.css">
<script src="js/codemirror/lib/codemirror.js"></script>
<script src="js/codemirror/mode/javascript/javascript.js"></script>
<div class="snippetList" id="sl-recent">

    <%
        long startTime = new Date().getTime();

        String s = request.getParameter("s");

        if (s == null || s.trim().length() == 0) {
            out.print("Die Suchanfrage ist leer, Sie lappen.");
            return;
        }

        String[] searchStrings = s.split(" ");
        String titleSearch = "";
        String tagSearch = "";
        String userSearch = "";

        Connection conn = DBCONN.getConnection();

        for (String searchString : searchStrings) {
            titleSearch += " LOWER(snippet.title) LIKE LOWER('%" + searchString + "%') OR ";
            tagSearch += " LOWER(tags.tag) LIKE LOWER('%" + searchString + "%') OR ";

            userSearch += " LOWER(user.username) LIKE LOWER('%" + searchString + "%') OR ";
            userSearch += " LOWER(user.firstname) LIKE LOWER('%" + searchString + "%') OR ";
            userSearch += " LOWER(user.lastname) LIKE LOWER('%" + searchString + "%') OR ";
            userSearch += " LOWER(user.email) LIKE LOWER('" + searchString + "') OR ";
        }

        titleSearch = titleSearch.substring(0, titleSearch.length() - 3);
        tagSearch = tagSearch.substring(0, tagSearch.length() - 3);
        userSearch = userSearch.substring(0, userSearch.length() - 3);

        String sql = "SELECT snippet.snippetid as ID, snippet.created FROM snippet "
                + "LEFT JOIN snippettag on snippet.snippetid = snippettag.snippetid "
                + "LEFT JOIN tags on snippettag.tagid = tags.tagid "
                + "JOIN user on snippet.owner = user.userID "
                + "JOIN snippetmeta ON snippet.snippetid = snippetmeta.owner "
                + /* searchstring = Mobile Navigation*/ "WHERE "
                + "( "
                + titleSearch
                + "OR ( "
                + tagSearch
                + " ) "
                + "OR "
                + userSearch
                + " ) AND "
                + "(snippetmeta.metakey = 'ISPUBLIC' AND snippetmeta.metavalue = 1) ORDER BY snippet.created DESC";

        List<Integer> snippetIds = new ArrayList<Integer>();
        Statement stmt = conn.createStatement();
        ResultSet set = stmt.executeQuery(sql);
        while (set.next()) {
            int id = set.getInt("ID");
            if (!snippetIds.contains(id)) {
                snippetIds.add(id);
            }
        }
        set.close();
        stmt.close();

        int results = snippetIds.size();
        long endTime = new Date().getTime();

        if (results > 0) {
            String restext = results > 1 ? " results" : " result";
            float time = (endTime - startTime) / 1000;
    %>
    <div class="searchMeta"><p><%= "Found " + results + restext + " within " + time + " seconds"%> </p></div>
    <%

        for (int snippetId : snippetIds) {
    %>
    <jsp:include page="functions/viewSnippet.jsp">
        <jsp:param name="snippet" value="<%= snippetId%>"></jsp:param>
    </jsp:include>
    <%
        }

    } else {
    %>
    <div class="page-section search-section">No results found... please try other keywords</div>
    <% }%>

</div>