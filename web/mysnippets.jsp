<%--
    Document   : mysnippets
    Created on : 18.02.2019, 20:53:20
    Author     : J
--%>

<%@page import="snippet.Authi.Auth"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if (!Auth.isLoggedIn(session)) {
        return;
    }

    String view = request.getParameter("view");
    if (view == null || view.equals("overview")) {
%>
<%@ include file="mysnippets/overview.jsp" %>
<%
} else if (view.equals("snippet")) {
%>
<%@ include file="mysnippets/snippet.jsp" %>
<%} else {
%>
<%@ include file="error/404.jsp" %>
<%
    }
%>