<%--
    Document   : index
    Created on : 12.01.2019, 18:14:01
    Author     : Alexander
--%>


<%@page import="snippet.Authi.User"%>
<%@page import="java.util.List"%>
<%@include file="init.jsp" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%    List<User> users = User.GetAll(User.class);
    if (users.size() == 0) {
        response.sendRedirect("install.jsp");
    }

    String searchtext = request.getParameter("s");
    if (searchtext == null) {
        searchtext = "";
    }
%>
<!DOCTYPE html>
<html lang="DE">
    <!-- Meta -->
    <head>
        <meta charset="utf-8">
        <title>Snippet</title>
        <meta name="description" content="Snippet Frontend">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet' type='text/css'>
        <% out.print("<script src=\"js/jquery.js\"></script>");%>

        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/style.css">

        <link href="css/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
        <!-- Shims -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/pnotify.custom.min.js"></script>
        <script src="js/basic.js"></script>

        <!-- Web App -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="white">

    </head>

    <body>
        <div id="wrapper">
            <div class="inside">

                <aside id="nav" class="left">

                    <div class="top">

                        <div class="logo">
                            <h1><a href="index.jsp">snippet</a></h1>
                            <p>your code database.</p>
                        </div>

                        <div class="nav-btn">
                            <span class="bar"></span>
                            <span class="bar"></span>
                            <span class="bar"></span>
                        </div>

                    </div>

                    <nav>
                        <jsp:include page="menu.jsp" flush="true" />
                    </nav>
                </aside>

                <div id="content" class="container">

                    <header>
                        <div class="search-box">
                            <form method="get">
                                <div class="widget">
                                    <input type="hidden" name="nav" value="search">
                                    <input type="search" placeholder="Search..." value="<%= searchtext%>" name="s" class="search-text" />
                                </div>
                                <div class="widget search-submit">
                                    <input type="submit" value="Search" class="btn search" />
                                </div>
                            </form>
                        </div>
                        <nav id="toolbar" class="no-mobile">
                            <jsp:include page="menu.jsp" flush="true" />
                        </nav>
                    </header>

                    <!-- CONTENT HERE -->
                    <div class="page-content"> <!-- Content for Textpages !-->
                        <jsp:include page="content.jsp" flush="true" />
                    </div>
                    <!-- CONTENT END -->
                </div>

            </div>

        </div>

        <script src="js/clipboard.min.js"></script>
        <script>
            new Clipboard('.copy-clipboard');
            new Clipboard('.copy-url');
        </script>
        <link rel="stylesheet" href="js/codemirror/lib/codemirror.css">
        <script src="js/codemirror/lib/codemirror.js"></script>
        <script src="js/codemirror/mode/javascript/javascript.js"></script>
        <script>
            $('.read_snippet_code').each(function () {
                var editor = CodeMirror.fromTextArea($(this)[0], {
                    lineNumbers: true,
                    mode: "javascript",
                    readOnly: true
                });
            });
        </script>
    </body>

</html>