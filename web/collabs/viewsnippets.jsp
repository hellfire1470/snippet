<%--
    Document   : viewsnippets
    Created on : 18.02.2019, 16:57:45
    Author     : Alexander
--%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.List"%>
<%@page import="snippet.lib.Snippet"%>
<%@page import="snippet.Authi.User"%>
<%@page import="snippet.Authi.Auth"%>
<%@page import="snippet.lib.Collab"%>

<%
    String cid = request.getParameter("cid");
    if (cid == null || cid.trim().length() == 0) {
        return;
    }

    int collabid = Integer.parseInt(cid);

    Collab collab = Collab.GetByID(Collab.class, collabid);

    if (!collab.isLoaded()) {
        return;
    }

    if (Auth.isLoggedIn(session)
            && collab.ContainUser(Auth.getUser(session))
            && collab.GetUserMeta(Auth.getUser(session), "VIEWSNIPPETS").equals("1")) {
        List<Snippet> snippets = collab.GetSnippets();
        if (snippets.size() > 0) {
            for (Snippet snippet : snippets) {
%>
<jsp:include page="/functions/viewSnippet.jsp">
    <jsp:param name="snippet" value="<%= snippet.GetID()%>"></jsp:param>
</jsp:include>
<%
        }
    }
    //return;
} else {
    int countSnippets = 0;
    if (collab.GetSnippets().size() > 0) {
        for (Snippet snippet : collab.GetSnippets()) {
            if (snippet.IsPublic()) {
                countSnippets++;
%>
<jsp:include page="/functions/viewSnippet.jsp">
    <jsp:param name="snippet" value="<%= snippet.GetID()%>"></jsp:param>
</jsp:include>
<%
                }
            }
        }

        if (countSnippets > 0) {
            return;
        }
    }
%>

<div class="page-section error-section"><p>This collaboration has no snippets</p></div>
