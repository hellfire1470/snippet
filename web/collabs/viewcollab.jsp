<%--
    Document   : viewcollab.jsp
    Created on : 18.02.2019, 17:38:25
    Author     : Alexander
--%>
<%@page import="snippet.lib.Collab"%>
<script>
    var leaveCollab = function (cname, cid) {
        if (confirm('Are you sure you want to leave ' + cname)) {
            $.ajax({
                url: "Collab",
                type: "POST",
                dataType: "json",
                data: {action: "leave", collab: cid}
            }).done(function (result) {
                if (result.success == 1) {
                    location.reload();
                } else {
                    error(result.error);
                }
            });
        }
    }
    var joinCollab = function (cid) {
        $.ajax({
            url: "Collab",
            type: "POST",
            dataType: "json",
            data: {action: "join", collab: cid}
        }).done(function (result) {
            if (result.success == 1) {
                location.reload();
            } else {
                error(result.error);
            }
        });
    }
</script>
<%
    int collabid = Integer.parseInt(request.getParameter("collab"));
    Collab collab = Collab.GetByID(Collab.class, collabid);
    if (collab.isLoaded()) {
%>
<jsp:include page="/functions/viewCollab.jsp">
    <jsp:param name="collab" value="<%= collab.GetID()%>"></jsp:param>
</jsp:include>
<%
    }

%>
