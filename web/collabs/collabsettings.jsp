<%--
    Document   : collabsettings
    Created on : 18.02.2019, 21:07:06
    Author     : Alexander
--%>

<%@page import="snippet.Authi.User"%>
<%@page import="snippet.lib.Collab"%>
<%@page import="snippet.Authi.Auth"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String _cid = request.getParameter("cid");
    if (_cid == null) {
        return;
    }

    int cid = 0;

    try {
        cid = Integer.parseInt(_cid);
    } catch (Exception e) {
        return;
    }

    if (!Auth.isLoggedIn(session)) {
        return;
    }

    Collab collab = Collab.GetByID(Collab.class, cid);
    if (collab == null || !collab.isLoaded()) {
        return;
    }

    if (!collab.GetUserMeta(Auth.getUser(session), "ADMIN").equals("1")) {
        return;
    }

    String userpermission = request.getParameter("userpermissions");
    String _uid = request.getParameter("uid");
    if (userpermission != null && _uid != null) {
        int uid = 0;
        try {
            uid = Integer.parseInt(_uid);
        } catch (Exception e) {
            return;
        }
        User user = User.GetByID(User.class, uid);
        String admin = request.getParameter("admin");
        String viewsnippets = request.getParameter("viewsnippets");
        String viewusers = request.getParameter("viewusers");
        String writesnippets = request.getParameter("writesnippets");

        if (user != null && user.isLoaded()) {
            collab.SetUserMeta(user, "ADMIN", admin == null ? "0" : "1");
            collab.SetUserMeta(user, "VIEWSNIPPETS", viewsnippets == null ? "0" : "1");
            collab.SetUserMeta(user, "VIEWUSERS", viewusers == null ? "0" : "1");
            collab.SetUserMeta(user, "WRITESNIPPETS", writesnippets == null ? "0" : "1");
        }

    }

    String collabsettings = request.getParameter("collabsettings");
    if (collabsettings != null) {
        collab.SetMeta("ISPUBLIC", request.getParameter("ispublic") == null ? "0" : "1");
        collab.SetMeta("ISPUBLICLYLISTED", request.getParameter("ispubliclylisted") == null ? "0" : "1");
        collab.saveMeta();
    }

    if (request.getParameter("close") != null) {
        collab.delete();
        return;
    }

    if (request.getParameter("basicinfo") != null) {
        collab.SetAttr("description", request.getParameter("description"));
        if (collab.save()) {
            out.print("<script>success(\"Collab description saved\");</script>");
        } else {
            out.print("<script>error(\"failed tos save collab description\");</script>");
        }
    }

    String collabPublic = "";
    if (collab.HasMeta("ISPUBLIC")) {
        collabPublic = collab.GetMeta("ISPUBLIC").equals("1") ? "checked" : "";
    }
    String isPubliclyListed = "";
    if (collab.HasMeta("ISPUBLICLYLISTED")) {
        isPubliclyListed = collab.GetMeta("ISPUBLICLYLISTED").equals("1") ? "checked" : "";
    }
%>
<div class="singleCollab collabSettings">
    <h1>Settings: <%= collab.GetName()%></h1>
    <h1>Collab description</h1>
    <div class="page-section settings-section description">
        <form method="post">
            <textarea name="description" placeholder="Collab description..."><%= collab.GetDescription()%></textarea>
            <input type="submit" class="btn submit green" name="basicinfo" value="save">
        </form>
    </div>

    <h1>Privacy settings</h1>
    <div class="page-section settings-section privacy">
        <form method="post">

            <fieldset class="checkbox_container">
                <span>
                    <input type="checkbox" name="ispublic" id="ispublic" <%= collabPublic%>>
                    <label for="ispublic">Collab is public.</label>
                </span>
            </fieldset>

            <fieldset class="checkbox_container">
                <span>
                    <input type="checkbox" name="ispubliclylisted" id="ispubliclylisted" <%= isPubliclyListed%>>
                    <label for="ispubliclylisted">Collab is publicly listed.</label>
                </span>
            </fieldset>
            <input type="submit" class="btn submit green" value="save" name="collabsettings">
        </form>
    </div>
    <h1>User permissions</h1>
    <div class="page-section settings-section users">
        <p>You can change any user's permissions in your collab by using the checkboxes below.</p>
        <%
            for (User user : collab.GetUsers()) {
                String userAdmin = "";
                if (collab.HasUserMeta(user, "ADMIN")) {
                    userAdmin = collab.GetUserMeta(user, "ADMIN").equals("1") ? "checked" : "";
                }
                String userViewsnippets = "";
                if (collab.HasUserMeta(user, "VIEWSNIPPETS")) {
                    userViewsnippets = collab.GetUserMeta(user, "VIEWSNIPPETS").equals("1") ? "checked" : "";
                }
                String userViewUsers = "";
                if (collab.HasUserMeta(user, "VIEWUSERS")) {
                    userViewUsers = collab.GetUserMeta(user, "VIEWUSERS").equals("1") ? "checked" : "";
                }
                String userWriteSnippets = "";
                if (collab.HasUserMeta(user, "WRITESNIPPETS")) {
                    userWriteSnippets = collab.GetUserMeta(user, "WRITESNIPPETS").equals("1") ? "checked" : "";
                }
        %>

        <div class="singleUser">
            <p class="label-editable"><%= user.GetFirstName() + " " + user.GetLastName()%></p>
            <div class="userSettings">
                <!--<form method="post">-->

                <% if (Auth.getUser(session).GetID() != user.GetID()) {%>
                <fieldset class="checkbox_container">
                    <span>
                        <input type="checkbox" id="admin-<%= user.GetID()%>" name="admin" <%= userAdmin%> onchange="javascript: updateUserPermission('<%= user.GetID()%>', 'ADMIN', $(this).is(':checked'));"><label for="admin-<%= user.GetID()%>">is admin</label>
                    </span>
                </fieldset>
                <% }%>
                <fieldset class="checkbox_container">
                    <span>
                        <input type="checkbox" id="viewsnippets-<%= user.GetID()%>" name="viewsnippets" <%= userViewsnippets%> onclick="javascript: updateUserPermission('<%= user.GetID()%>', 'VIEWSNIPPETS', $(this).is(':checked'));"><label for="viewsnippets-<%= user.GetID()%>">view snippets</label>
                    </span>
                </fieldset>
                <fieldset class="checkbox_container">
                    <span>
                        <input type="checkbox" id="viewusers-<%= user.GetID()%>" name="viewusers" <%=userViewUsers%> onclick="javascript: updateUserPermission('<%= user.GetID()%>', 'VIEWUSERS', $(this).is(':checked'));"><label for="viewusers-<%= user.GetID()%>">view users</label>
                    </span>
                </fieldset>
                <fieldset class="checkbox_container">
                    <span>
                        <input type="checkbox" id="writesnippets-<%= user.GetID()%>" name="writesnippets" <%= userWriteSnippets%> onclick="javascript: updateUserPermission('<%= user.GetID()%>', 'WRITESNIPPETS', $(this).is(':checked'));"><label for="writesnippets-<%= user.GetID()%>">write snippets</label>
                    </span>
                </fieldset>
                <% if (Auth.getUser(session).GetID() != user.GetID()) {%>
                <a href="#" class="btn green" onclick="javascript: removeFromCollab('<%= user.GetUsername()%>', '<%=collab.GetID()%>'); return false;">Kick <%= user.GetFirstName()%></a>
                <% } %>
                <!--</form>-->
            </div>
        </div>
        <%
            }
        %>
    </div>

    <h1>Invite user</h1>
    <div class="page-section settings-section invite">
        <p>You can invite users to this collab by entering their e-mail address in the form below.</p>
        <form method="post">
            <input type="text" name="email" placeholder="user@mail.com">
            <input type="submit" class="btn submit green" name="invite" value="invite user">
        </form>
    </div>

    <h1>Delete collab</h1>
    <div class="page-section settings-section close">
        <p>If you want to delete this collab, click the button below.<br />Please be aware that this action is irreversible.</p>
        <form method="post">
            <input type="submit" class="btn submit green" value="delete this collab" name="close">
        </form>
    </div>
</div>

<script>
    var updateUserPermission = function (uid, permission, value) {
        $.ajax({
            url: 'Collab',
            type: 'POST',
            dataType: "json",
            data: {action: 'update', target: 'user', collab: '<%= collab.GetID()%>', user: uid, key: permission, value: value}
        }).done(function (result) {
            if (result.success == 1) {
                success('Permissions changed', 2000);
            } else {
                error('Something went wrong', 6000);
            }
        });
    }
</script>
