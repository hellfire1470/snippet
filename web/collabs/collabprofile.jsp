<%--
    Document   : collabprofile
    Created on : 18.02.2019, 20:45:47
    Author     : Alexander
--%>
<%@page import="snippet.lib.Collab"%>
<%
    String cid = request.getParameter("cid");
    if (cid == null || cid.trim().length() == 0) {
        return;
    }

    Collab collab = Collab.GetByID(Collab.class, Integer.parseInt(cid));

    if (!collab.isLoaded()) {
        return;
    }
%>

<div class="collabDetail">
    <h1>More about this collab</h1>
    <jsp:include page="/collabs/viewcollab.jsp">
        <jsp:param name="collab" value="<%= collab.GetID()%>"></jsp:param>
    </jsp:include>

    <h1>Members of <%= collab.GetName()%></h1>
    <jsp:include page="/collabs/viewusers.jsp">
        <jsp:param name="collab" value="<%= collab.GetID()%>"></jsp:param>
    </jsp:include>

    <h1>Snippets</h1>
    <jsp:include page="/collabs/viewsnippets.jsp">
        <jsp:param name="collab" value="<%= collab.GetID()%>"></jsp:param>
    </jsp:include>
</div>