<%--
    Document   : content
    Created on : 13.01.2019, 00:18:48
    Author     : Alexander
--%>
<%@page import="snippet.Helper"%>

<%
    String[] allowed_navigation = new String[]{"browsesnippets", "browsecollabs", "mysnippets", "writesnippet", "registration", "search", "profile", "collabs", "viewsnippet"};

    String navigation = "browsesnippets";
    if (request.getParameter("nav") != null) {
        navigation = request.getParameter("nav");
    }

    if (Helper.in_array(navigation, allowed_navigation)) {
        pageContext.include(navigation + ".jsp");
    }
%>