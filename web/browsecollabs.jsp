<%--
    Document   : browsecollabs
    Created on : 18.02.2019, 16:28:54
    Author     : Alexander
--%>
<%@page import="snippet.lib.Collab"%>
<%@page import="snippet.Authi.Auth"%>
<%@page import="snippet.Authi.Group"%>
<%@page import="snippet.Authi.User"%>
<%
    String view = request.getParameter("view");
    if (view != null && view.trim().length() > 0) {
        if (view.equals("users")) {
%>
<%@include file="collabs/viewusers.jsp" %>
<%
    return;
} else if (view.equals("snippets")) {
%>
<%@include file="collabs/viewsnippets.jsp" %>
<%            return;
        }
    }

    String submit = request.getParameter("submit");
    String action = request.getParameter("action");

    if (submit != null && submit.trim().length() > 0
            && action != null && action.trim().length() > 0) {

        if (action.equals("au")) {

            String email = request.getParameter("email");
            String group = request.getParameter("group");
            User user = User.GetByTag(User.class, "email", email);
            Group collab = Group.GetByID(Group.class, Integer.parseInt(group));
            if (Auth.getUser(session).IsInGroup(collab)) {
                collab.AddUser(user);
            }
        } else if (action.equals("cg")) {

            String name = request.getParameter("name");
            String description = request.getParameter("description");
            if (name != null && name.trim().length() > 0
                    && description != null && description.trim().length() > 0) {
                Group collab = new Group();
                collab.SetAttr("name", name);
                collab.SetAttr("description", description);
                if (collab.create()) {
                    User u = Auth.getUser(session);
                    collab.AddUser(u);
                    collab.SetUserMeta(u, "ADMIN", "1");
                    collab.SetUserMeta(u, "WRITESNIPPETS", "1");
                    collab.SetUserMeta(u, "VIEWUSERS", "1");
                    collab.SetUserMeta(u, "VIEWSNIPPETS", "1");
                }
            }
        }
    }
%>

<div class="page-content">
    <h1>All collabs</h1>
    <div class="collab-area">
        <div class="collab-list all-collabs">

            <%
                for (Collab collab : Collab.GetAllPubliclyListed("ORDER BY name ASC")) {
            %>
            <jsp:include page="/collabs/viewcollab.jsp">
                <jsp:param name="collab" value="<%= collab.GetID()%>"></jsp:param>
            </jsp:include>
            <%
                }
            %>
        </div>
    </div>
    <%
        if (!Auth.isLoggedIn(session)) {
            return;
        }
    %>
</div>
