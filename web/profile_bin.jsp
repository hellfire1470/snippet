<%--
    Document   : profile_bin
    Created on : 13.02.2019, 15:12:11
    Author     : J
--%>
<%@page import="snippet.Authi.*" %>
<%
    String info = request.getParameter("info");
    String userID = request.getParameter("userid");
    String placeholder = request.getParameter("placeholder");
    String _info = info.replace(" ", "_");
    User user = (User) DBClass.GetByID(User.class, Integer.parseInt(userID));

    String editable = "";
    if (Auth.isLoggedIn(session) && Auth.getUser(session).GetID() == user.GetID()) {
        editable = "";
    }

    String metaInfo = (user.HasMeta(_info) ? user.GetMeta(_info) : placeholder);

%>
<div class="label-editable"><%= info%></div>
<div class="<%= editable%> editable" id="<%= _info%>"><%= metaInfo%></div>
