
var updateSnippetShare = function (id, ispublic, success, error) {
    $.ajax({
        url: "mysnippets/snippet_handler.php",
        type: "POST",
        data: {action: "updateshare", id: id, ispublic: ispublic}
    }).done(function (result) {
        var result = $.parseJSON(result);
        if (result.success == 1) {

            success();

        } else {

            error(result.error);
        }
    });
}

var forking = false;
var fork = function (snippet) {
    if (!forking) {
        forking = true;
        $.ajax({
            url: "Snippet",
            type: "POST",
            dataType: "json",
            data: {action: "fork", snippet: snippet}
        }).done(function (result) {
            if (result.success == 1) {

                var snippetid = result.id;
                location.href = "?nav=mysnippets&view=snippet&id=" + snippetid;
                forking = false;

            } else {
                $('.fork').html("Fork");
                forking = false;
                //error(result.error);
            }
        });
    }
}

var removing_from_collab = false;
var removeFromCollab = function (user, collab) {
    if (!removing_from_collab) {
        removing_from_collab = true;
        $.ajax({
            url: "ajax/collabs.php",
            type: "POST",
            data: {action: "remove", user: user, collab: collab}
        }).done(function (result) {
            var result = $.parseJSON(result);
            if (result.success == 1) {
                location.reload();
                removing_from_collab = false;

            } else {
                removing_from_collab = false;
                //error(result.error);
            }
        });
    }
}

consume_alert();
function consume_alert() {
    window.alert = function (message) {
        new PNotify({
            text: message
        });
    };
}

PNotify.prototype.options.delay = 4000;
var notify = function (content, type, delay) {
    if (delay != undefined) {
        PNotify.prototype.options.delay = delay;
    }
    var opts = {
        title: content,
        type: type,
        icon: ''
    };
    new PNotify(opts);
}

var rdy = false;
$(document).ready(function () {
    rdy = true;
});
var error = function (content, delay) {
    content = "(╯°□°）╯︵ ┻━┻<br>" + content;
    if (rdy) {
        notify(content, 'error', delay);
    } else {
        $(document).ready(function () {
            notify(content, 'error', delay);
        });
    }
}

var success = function (content, delay) {
    if (rdy) {
        notify(content, 'success', delay);
    } else {
        $(document).ready(function () {
            notify(content, 'success', delay);
        });
    }
}

function hideDiv() {
    $(window).width() > 1167 ? $("aside nav").show() : $("aside nav").hide()
}
function expandedRemover() {
    $("aside nav").removeClass("expanded")
}
!function () {
    function t() {}
    function e(t) {
        return o.retinaImageSuffix + t
    }
    function i(t, i) {
        if (this.path = t || "", "undefined" != typeof i && null !== i)
            this.at_2x_path = i, this.perform_check = !1;
        else {
            if (void 0 !== document.createElement) {
                var n = document.createElement("a");
                n.href = this.path, n.pathname = n.pathname.replace(h, e), this.at_2x_path = n.href
            } else {
                var a = this.path.split("?");
                a[0] = a[0].replace(h, e), this.at_2x_path = a.join("?")
            }
            this.perform_check = !0
        }
    }
    function n(t) {
        this.el = t, this.path = new i(this.el.getAttribute("src"), this.el.getAttribute("data-at2x"));
        var e = this;
        this.path.check_2x_variant(function (t) {
            t && e.swap()
        })
    }
    var a = "undefined" == typeof exports ? window : exports, o = {retinaImageSuffix: "@2x", check_mime_type: !0, force_original_dimensions: !0};
    a.Retina = t, t.configure = function (t) {
        null === t && (t = {});
        for (var e in t)
            t.hasOwnProperty(e) && (o[e] = t[e])
    }, t.init = function (t) {
        null === t && (t = a);
        var e = t.onload || function () {};
        t.onload = function () {
            var t, i, a = document.getElementsByTagName("img"), o = [];
            for (t = 0; t < a.length; t += 1)
                i = a[t], i.getAttributeNode("data-no-retina") || o.push(new n(i));
            e()
        }
    }, t.isRetina = function () {
        var t = "(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)";
        return a.devicePixelRatio > 1 ? !0 : a.matchMedia && a.matchMedia(t).matches ? !0 : !1
    };
    var h = /\.\w+$/;
    a.RetinaImagePath = i, i.confirmed_paths = [], i.prototype.is_external = function () {
        return!(!this.path.match(/^https?\:/i) || this.path.match("//" + document.domain))
    }, i.prototype.check_2x_variant = function (t) {
        var e, n = this;
        return this.is_external() ? t(!1) : this.perform_check || "undefined" == typeof this.at_2x_path || null === this.at_2x_path ? this.at_2x_path in i.confirmed_paths ? t(!0) : (e = new XMLHttpRequest, e.open("HEAD", this.at_2x_path), e.onreadystatechange = function () {
            if (4 !== e.readyState)
                return t(!1);
            if (e.status >= 200 && e.status <= 399) {
                if (o.check_mime_type) {
                    var a = e.getResponseHeader("Content-Type");
                    if (null === a || !a.match(/^image/i))
                        return t(!1)
                }
                return i.confirmed_paths.push(n.at_2x_path), t(!0)
            }
            return t(!1)
        }, void e.send()) : t(!0)
    }, a.RetinaImage = n, n.prototype.swap = function (t) {
        function e() {
            i.el.complete ? (o.force_original_dimensions && (i.el.setAttribute("width", i.el.offsetWidth), i.el.setAttribute("height", i.el.offsetHeight)), i.el.setAttribute("src", t)) : setTimeout(e, 5)
        }
        "undefined" == typeof t && (t = this.path.at_2x_path);
        var i = this;
        e()
    }, t.isRetina() && t.init(a)
}(), $(document).ready(function () {
    hideDiv(), console.log("logging"), $("aside .nav-btn").click(function () {
        $("aside nav").stop(!0, !0).slideToggle(150), $("aside .nav-btn").toggleClass("open"), $("aside nav").toggleClass("expanded"), $("aside").toggleClass("expanded")
    })
});
var widthwdw = 0;
$(window).load(function () {
    widthwdw = $(window).width()
}), $(window).resize(function () {
    widthwdw != $(window).width() && (widthwdw = $(window).width(), hideDiv(), $("aside .nav-btn").removeClass("open"))
});

if ($(window).outerWidth() > 1167) {



} else {

}
