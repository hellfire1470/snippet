<%--
    Document   : writesnippet
    Created on : 16.02.2019, 19:27:45
    Author     : J
--%>

<%@page import="java.util.List"%>
<%@page import="snippet.lib.Tag"%>
<%@page import="snippet.Authi.Auth"%>
<%@page import="snippet.Authi.Group"%>
<%@page import="snippet.lib.Language"%>
<%@page import="snippet.lib.SnippetUser"%>
<%@page import="snippet.lib.Snippet"%>
<%@page import="snippet.Authi.DBClass"%>

<%
    if (!Auth.isLoggedIn(session)) {
        out.print("Ouh no! Login first.");
        return;
    }
%>
<link rel="stylesheet" href="js/codemirror/lib/codemirror.css">
<script src="js/codemirror/lib/codemirror.js"></script>
<script src="js/codemirror/mode/javascript/javascript.js"></script>
<h1>Write a new Snippet</h1>

<div class="snippetList" id="sl-new">
    <div class="snippetSingle snippetNew" id="write_snippet">
        <div class="snippetTitle">
            <input type="text" class="text snippetTitleInput" placeholder="Name ..." id="write_snippet_name">
        </div>
        <div class="snippetCode">
            <textarea id="write_snippet_code" name="write_snippet_code" placeholder="Do the magic here" style="display:none;"></textarea>
        </div>
        <div class="snippetSettings">
            <div class="snippetShare">
                <h4>Share with<h4>
                        <select id="share_with">
                            <option value="-" selected>-</option>
                            <%
                                for (Group group : Auth.getUser(session).GetGroups()) {
                                    if (group.GetUserMeta(Auth.getUser(session), "WRITESNIPPETS").equals("1")) {
                            %>
                            <option value="<%= group.GetID()%>" selected><%= group.GetName()%></option>
                            <%      }
                                }
                            %>
                        </select>
                        </div>
                        <div class="snippetLanguage">
                            <h4>Language</h4>
                            <select id="write_snippet_language">
                                <%                                    for (Language lang : Language.GetLanguages()) {
                                %>
                                <option value="<%= lang.GetID()%>"><%= lang.GetName()%></option>';
                                <%
                                    }
                                %>
                            </select>
                        </div>
                        <fieldset class="checkbox_container snippetPublic">
                            <h4>Visibility</h4>
                            <input type="checkbox" id="write_snippet_public"><label for="write_snippet_public">Share snippet publicly.</label>
                        </fieldset>
                        <div id="write_snippet_error"></div>
                        </div>
                        <div class="snippetTags">
                            <input class="text txt_tag list-text" list="tags" placeholder="tag" class="txt_tag">
                            <datalist id="tags" class="text datalist">
                                <%
                                    List<Tag> dbc = Tag.GetAll(Tag.class);
                                    for (Tag snippettag : dbc) {
                                %>
                                <option value="<%= snippettag.GetAttr("tag")%>">
                                    <%
                                        }
                                    %>
                            </datalist>
                            <button id="btn_tag_add" class="btn green submit">Add Hashtag</button>
                        </div>
                        <div class="snippetCreate">
                            <input type="button" id="create_snippet" class="btn green submit" value="Create">
                        </div>
                        </div>

                        <script>

                            $('#create_snippet').on('click', function () {
                                var snippet_language = $('#write_snippet_language').val();
                                var snippet_name = $('#write_snippet_name').val();
                                var snippet_code = editor.getValue();
                                var is_public = $('#write_snippet_public').prop("checked");
                                var tags = [];
                                var share_with = $('#share_with').val();
                                $('.txt_tag').each(function (i) {
                                    if ($(this).val().length > 0) {
                                        tags.push($(this).val());
                                    }
                                });
                                if (snippet_name == "" || snippet_code == "") {
                                    error("Some fields are empty");
                                } else {
                                    createSnippet(snippet_name, snippet_code, snippet_language, is_public, tags, share_with);
                                }
                            });

                            var creating = false;
                            var createSnippet = function (name, code, language, ispublic, tags, share_with) {
                                if (!creating) {
                                    creating = true;
                                    $.ajax({
                                        url: "Snippet",
                                        type: "POST",
                                        dataType: "json",
                                        data: {action: "create", name: name, code: code, language: language, ispublic: ispublic, tags: tags, share_with: share_with}
                                    }).done(function (result) {
                                        if (result.success) {
                                            // Login Successfull
                                            creating = false;
                                            location.href = "?nav=mysnippets";
                                        } else {
                                            // Login Failed
                                            creating = false;
                                            error(result.error);
                                        }
                                    });
                                }
                            };

                            var editor = CodeMirror.fromTextArea(document.getElementById("write_snippet_code"), {
                                lineNumbers: true,
                                mode: "javascript"
                            });

                            $('#btn_tag_add').click(function () {
                                $('<input list="tags" placeholder="#tag" class="text txt_tag list-text">').insertAfter($('.txt_tag').last());
                            });

                        </script>