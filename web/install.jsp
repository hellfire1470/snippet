<%--
    Document   : install
    Created on : 11.03.2019, 16:35:31
    Author     : J
--%>
<%@page import="java.util.List"%>
<%@page import="snippet.Authi.User"%>
<%@page import="snippet.Authi.RegistrationResult"%>
<%@page import="snippet.Authi.Auth"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="snippet.Authi.DBCONN"%>
<%@page import="java.io.File"%>
<%@page import="javax.servlet.http.HttpServletRequest"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String navGet = request.getParameter("nav");
    List<User> users = User.GetAll(User.class);
    if (users.size() > 0) {
        response.sendRedirect("index.jsp");
    }
%>
<html lang="DE">
    <!-- Meta -->
    <head>
        <meta charset="utf-8">
        <title>Snippet Installation</title>
        <meta name="description" content="Snippet Frontend">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Web App -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="white">

    </head>

    <body>
        <div id="wrapper">
            <div class="box centered" id="installmain">
                <div class="logo">
                    <h1><a href="/index.jsp">snippet</a></h1>
                    <p>your code database.</p>
                </div>
                <div class="box-content" id="installcontent">
                    <%= showStep(request)%>
                </div>
            </div>
        </div>

    </body>
</html>
<%!
    Boolean nextStep = false;

    public String showFail(String errormsg) {
        return "<div class=\"checkit\"><img class=\"sucfailgfx\" src=\"gfx/fail.png\"><div class=\"failmsg\">" + errormsg + "</div></div>";
    }

    public String showSuccess(String successmsg) {
        return "<div class=\"checkit\"><img class=\"sucfailgfx\" src=\"gfx/success.png\">" + successmsg + "</div>";
    }

    public String showStep(HttpServletRequest request) {
        String nav = request.getParameter("nav");

        if (nav == null || nav.equals("step1")) {
            return getStep1();
        } else if (nav.equals("step2")) {
            return getStep2();
        } else if (nav.equals("step3")) {
            return getStep3(request);
        }

        return getStep1();
    }

    public String getStep1() {
        nextStep = false;
        String res = "";
        try {
            res += "<h3>Willkommen</h3><p>zum Installationswizard von <em>Snippet</em>.<br />Dieser Assistent wird jetzt Snippet V0.01 auf Ihrem System installieren.</p><p>Drücken Sie auf \"Weiter\" um Fortzufahren.</p><nav class=\"bottomnav\"><ul><li><a href=\"install.jsp?nav=step2\">Weiter</a></li></ul></nav>";
        } catch (Exception e) {
        }
        return res;
    }

    public String getStep2() {
        String res = "<h3>Überprüfen der lokalen Dateien</h3>"
                + "<p>In diesem Schritt werden die Rechte und die Vollständigkeit der Installation getestet. Dies kann einen Moment dauern.</p>";

        boolean everything_is_fine = true;

        res += "<h4>Prüfe Design Verzeichnisse...</h4>" + showSuccess("<p>Erfolgreich</p>");
        res += "<h4>Prüfe Snippet Library Verzeichnis...</h4>" + showSuccess("<p>Erfolgreich</p>");
        res += "<h4>Prüfe Image Verzeichnis...</h4>" + showSuccess("<p>Erfolgreich</p>");

        res += "<button class=\"btn green submit\" onclick=\"location.reload();\" id=\"reload\">Erneut versuchen</button>";
        res += "<nav id=\"bottomnav\"><ul><li><a href=\"install.jsp?nav=step1\">Zurück</a></li> ";

        if (everything_is_fine) {
            res += "<li><a href=\"install.jsp?nav=step3\">Weiter</a></li></ul>";
        } else {
            res += "<li>Weiter</li></ul>";
        }
        return res;
    }

    public boolean checkExistance(String filepath, String filename) {
        File file = new File(filepath + filename);
        return (file.exists() || file.isDirectory());
    }

    public boolean checkExistance(String filepath) {
        return checkExistance(filepath, "");
    }

    public String getStep3(HttpServletRequest req) {
        String res = "<h3>Installation erfolgreich!</h3>"
                + "<p>Die Installation ist nun weitesgehend abgeschlossen, und wenn Sie wollen können Sie sich nun hier noch einen User erstellen - dies können Sie aber auch jeder Zeit über die normale Registrierung auf der Snippetseite nachholen."
                + "Wollen Sie sich nun einen Account <a href='install.jsp?nav=step3&action=register'>erstellen</a>?</p>";

        String action = req.getParameter("action");
        String ready = req.getParameter("ready");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String email = req.getParameter("email");
        String firstname = req.getParameter("firstname");
        String secondname = req.getParameter("secondname");
        if (username == null) {
            username = "";
        }
        if (password == null) {
            password = "";
        }
        if (email == null) {
            email = "";
        }
        if (firstname == null) {
            firstname = "";
        }
        if (secondname == null) {
            secondname = "";
        }

        RegistrationResult rr = RegistrationResult.UNKNOWN_ERROR;

        if (ready != null) {
            rr = Auth.Register(username, password, email, firstname, secondname);
            if (rr == RegistrationResult.SUCCESS) {
                res += showSuccess("Account <p>Erfolgreich</p> erstellt");
                res += "<li><a href=\"login.jsp\">Weiter</a></li></ul>";
            } else {
                res += getRegistrationError(rr);
            }
        }

        if (action != null && action.equals("register") || (ready != null && rr != RegistrationResult.SUCCESS)) {
            res += "<form id=\"registerAcc\" method=\"POST\" action=\"install.jsp?nav=step3&ready\"><br>"
                    + "<input class=\"text\" type=\"text\" name=\"firstname\" value=\"" + firstname + "\" placeholder=\"Firstname\">"
                    + "<input class=\"text\" type=\"text\" name=\"secondname\" value=\"" + secondname + "\" placeholder=\"Lastname\">"
                    + "<input class=\"text\" type=\"username\" name=\"username\" value=\"" + username + "\" placeholder=\"Username\">"
                    + "<input class=\"text\" type=\"password\" name=\"password\" value=\"" + password + "\" placeholder=\"Password\">"
                    + "<input class=\"text\" type=\"email\" name=\"email\" value=\"" + email + "\" placeholder=\"Email Adress\">"
                    + "<div><input class=\"btn green submit\" type=\"submit\" id=\"submit\" value=\"Registrieren\"></div>"
                    + "</form>";
        }

        return res;
    }

    public String getRegistrationError(RegistrationResult rr) {
        String error = "";
        switch (rr) {
            case REQUIRED_EMAIL:
                error += showFail("<p>Bitte geben Sie eine Email Adresse an</p>");
                break;
            case INVALID_EMAIL:
                error += showFail("<p>Bitte geben Sie eine gültige Email Adresse an</p>");
                break;
            case USER_SHORT:
                error += showFail("<p>Benutzername ist zu kurz, bitte wählen Sie einen mit mindestens 4 Zeichen</p>");
                break;

            case USER_LONG:
                error += showFail("<p>Benutzername ist zu lang, bitte wählen Sie einen mit höchstens 32 Zeichen</p>");
                break;

            case USER_EXISTS:
                error += showFail("<p>Benutzername existiert bereits, bitte wählen Sie einen Anderen</p>");
                break;

            case PASSWORD_SHORT:
                error += showFail("<p>Passwort ist zu kurz, bitte wählen Sie ein Passwort mit mindestens 8 Zeichen</p>");
                break;

            case PASSWORD_LONG:
                error += showFail("<p>Passwort ist zu lang, bitte wählen Sie ein Passwort mit höchstens 32 Zeichen</p>");
                break;

            case REQUIRED_FIRSTNAME:
                error += showFail("<p>Bitte geben Sie einen Vornamen ein</p>");
                break;

            case REQUIRED_LASTNAME:
                error += showFail("<p>Bitte geben Sie einen Nachnamen ein</p>");
                break;

            case MYSQL_ERROR:
            case UNKNOWN_ERROR:
                error += showFail("<p>Ein unbekannter Fehler ist aufgetreten. Bitte kontaktieren Sie unseren Support unter: <a href=\"mailto:support@k3ks.de\">support@k3ks.de</a></p>");
                break;
        }
        return error;
    }
%>