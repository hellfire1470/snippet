<%--
    Document   : viewSnippet
    Created on : 13.02.2019, 15:08:15
    Author     : Alexander
--%>


<%@page import="snippet.lib.SnippetUser"%>
<%@page import="snippet.Authi.Auth"%>
<%@page import="snippet.lib.Snippet"%>
<%@page import="snippet.Authi.DBClass"%>
<%@include file="functions/snippet.jsp" %>
<%

    String _snippet = request.getParameter("snippet");
    if (_snippet == null) {
        return;
    }

    int snippetid = Integer.parseInt(_snippet);

    Snippet snippet = (Snippet) DBClass.GetByID(Snippet.class, snippetid);

    if (snippet == null) {
        return;
    }

    if (snippet.IsPublic()) {
%>
<jsp:include page="functions/viewSnippet.jsp">
    <jsp:param name="snippet" value="<%= snippetid%>"></jsp:param>
</jsp:include>
<%
        return;
    }

    if (Auth.isLoggedIn(session)) {
        SnippetUser user = (SnippetUser) Auth.getUser(session);

        if (UserHasPermissionToViewSnippet(session, user, snippet)) {
%>
<jsp:include page="functions/viewSnippet.jsp">
    <jsp:param name="snippet" value="<%= snippetid%>"></jsp:param>
</jsp:include>
<%
            return;
        }
    }

    // IF NO PERMISSION
%>
<%= "TODO:: include no permission site"%>

