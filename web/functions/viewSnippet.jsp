<%--
    Document   : viewSnippet
    Created on : 13.02.2019, 13:07:25
    Author     : Alexander
--%>

<%@page import="snippet.Authi.Auth"%>
<%@page import="snippet.lib.*"%>

<%
    Snippet snippet = new Snippet();
    snippet.loadByID(Integer.parseInt(request.getParameter("snippet")));
    if (!snippet.isLoaded()) {
        return;
    }
%>

<div class="snippetSingle" id="s<%= snippet.GetID()%>"> <!-- Snippet Single !-->

    <div class="singleLeft">

        <div class="singleTopLeft">

            <div class="author" id="originalAuthor"> <!-- Original Snippet Author !-->

                <!--<div class="snippetAuthorAvatar">
                        <a href="#"><img src="images/avatar-small.jpg" alt="User Profile" /></a> <!-- Link to Public Profile !-->
                <!--</div> -->
                <span><a href="?nav=profile&user=<%= snippet.GetCreator().GetUsername()%>"><%= snippet.GetCreator().GetFirstName() + " " + snippet.GetCreator().GetLastName()%></a></span>

            </div>

            <% if (snippet.IsForked()) {%>

            <div class="author forkedby" id="secAuthor"> <!-- If Snippet is Forked, User who forked it !-->
                <span><a href="?nav=profile&user=<%= snippet.GetForker().GetUsername()%>"><%= snippet.GetForker().GetFirstName() + " " + snippet.GetForker().GetLastName()%></a></span>
            </div>
            <div class="author forkedfrom" id="originalSnippet"> <!-- If Snippet is Forked, User who forked it !-->
                <span><a href="?nav=viewsnippet&snippet=<%= snippet.GetParentSnippet().GetID()%>"><%= snippet.GetParentSnippet().GetAttr("title")%></a></span>
            </div>

            <%
                }

                if (snippet.HasGroup()) {
            %>
            <div class="author team" id="team"> <!-- If Snippet is Forked, User who forked it !-->
                <span><a href="?nav=collabs&view=profile&cid=<%= snippet.GetGroup().GetID()%>"><%= snippet.GetGroup().GetName()%></a></span>
            </div>
            <%
                }
            %>

            <div class="snippetMeta">
                <p>Written by
                    <a href="?nav=profile&user=<%= snippet.GetCreator().GetUsername()%>"><%= snippet.GetCreator().GetFirstName() + " " + snippet.GetCreator().GetLastName()%></a>
                    <!--on <?php echo date("j. F H:i",strtotime($snippet->getAttr('created'))); ?>-->
                </p>
                <p>Language: <%= snippet.GetLanguage().GetName()%>
            </div>
        </div>

        <div class="singleContent">
            <div class="snippetTitle">
                <h3><%= snippet.GetAttr("title")%></h3>
            </div>

            <div class="snippetCode">
                <textarea class="read_snippet_code" name="read_snippet_code" style="display:none;"><%= snippet.GetAttr("code")%></textarea>
            </div>

        </div>

        <div class="singleLeftBottom">

            <div class="hashtags"> <!-- This snippets Hashtags !-->

                <% for (Tag tag : snippet.GetTags()) {%>
                <a href="?nav=search&s=<%= tag.GetAttr("tag")%>">#<%= tag.GetAttr("tag")%></a>
                <% }%>
            </div>
            <div class="copy">
                <a class="copy-url" href="#" onclick="javascript: success('Copied...'); return false;" data-clipboard-action="copy" data-clipboard-text="<%= request.getRequestURL()%>?nav=viewsnippet&snippet=<%= snippet.GetID()%>">copy URL</a>
                <a class="copy-clipboard" href="#" onclick="javascript: success('Copied...'); return false;" data-clipboard-action="copy" data-clipboard-text="<%= snippet.GetAttr("code")%>">copy code</a>
                <%
                    if (Auth.isLoggedIn(session)) {

                        // FORK BUTTON
%>
                <a href="#" class="fork" onclick="javascript: fork('<%= snippet.GetID()%>'); $(this).html('Forking ...'); return false;">Fork</a>
                <%

                    // EDIT BUTTON
                    if (Auth.getUser(session).GetID() == Integer.parseInt(snippet.GetAttr("creator"))) {
                %>
                <a href="?nav=mysnippets&view=snippet&id=<%= snippet.GetID()%>">Edit</a>

                <% }

                    }
                %>


            </div>

        </div>

    </div>

</div>