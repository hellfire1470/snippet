<%--
    Document   : snippet
    Created on : 13.02.2019, 12:54:20
    Author     : Alexander
--%>
<%@page import="snippet.Authi.Auth"%>
<%@page import="snippet.lib.SnippetUser"%>
<%@page import="snippet.lib.Snippet"%>
<%!
    /**
     * F�gt das Clipboard hinzu
     */
    void appendClipboard() {
%>
<script src="js/clipboard.min.js"></script>
<script>
    new Clipboard('.copy-clipboard');
    new Clipboard('.copy-url');
</script>
<%!
    }

    /**
     * F�gt den Editor hinzu
     */
    void appendEditor() {
%>
<link rel="stylesheet" href="js/codemirror/lib/codemirror.css">
<script src="js/codemirror/lib/codemirror.js"></script>
<script src="js/codemirror/mode/javascript/javascript.js"></script>
<script>
    $('.read_snippet_code').each(function () {
        var editor = CodeMirror.fromTextArea($(this)[0], {
            lineNumbers: true,
            mode: "javascript",
            readOnly: true
        });
    });
</script>

<%!
    }

    /**
     * Pr�ft ob ein User die Berechtigung dazu hat ein Snippet zu betrachten
     *
     * @param (Object)
     * @param (Snippet)
     */
    public boolean UserHasPermissionToViewSnippet(HttpSession session, SnippetUser user, Snippet snippet) {
        // SHOW IF PUBLIC
        if (snippet.IsPublic()) {
            return true;
        }

        if (snippet.HasGroup()) {
            if (snippet.GetGroup().ContainUser(user)) {
                return true;
            } else {
                return false;
            }
        }
        if (!Auth.isLoggedIn(session)) {
            return false;
        }
        if (Auth.getUser(session).GetID() == Integer.parseInt(snippet.GetAttr("creator"))) {
            return true;
        }
        return false;
    }

%>