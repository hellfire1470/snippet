<%--
    Document   : viewCollab
    Created on : 18.02.2019, 17:42:55
    Author     : Alexander
--%>

<%@page import="snippet.lib.SnippetUser"%>
<%@page import="snippet.Authi.Auth"%>
<%@page import="snippet.Authi.User"%>
<%@page import="snippet.lib.Collab"%>
<%
    Collab collab = new Collab();
    collab.loadByID(Integer.parseInt(request.getParameter("collab")));
    if (collab == null || !collab.isLoaded()) {
        return;
    }
%>

<div class="page-section collabSingle" id="collab_<%= collab.GetID()%>">
    <div class="collabInfo">

        <div class="collabTitle">
            <h3><%= collab.GetName()%></h3>
        </div>
        <div class="collabDescription">
            <h4>About this collab</h4>
            <%= collab.GetDescription()%>
        </div>
        <div class="collabAdmins">
            <h4>Administered by</h4>
            <ul>
                <%
                    for (SnippetUser user : collab.GetAdmins()) {
                %>
                <li><%= user.GetFirstName() + " " + user.GetLastName()%></li>
                    <%
                        }
                    %>

            </ul>
        </div>
    </div>
    <div class="collabActions">

        <div class="collabButtons">
            <a href="?nav=collabs&view=snippets&cid=<%= collab.GetID()%>">Snippets</a>

            <a href="?nav=collabs&view=profile&cid=<%= collab.GetID()%>">More info</a>
            <%
                if (Auth.isLoggedIn(session) && collab.IsUserAdmin(SnippetUser.Cast(SnippetUser.class, Auth.getUser(session)))) {
            %>
            <a href="?nav=collabs&view=settings&cid=<%= collab.GetID()%>">Settings</a>
            <%
                }
                if (Auth.isLoggedIn(session)) {
                    if (Auth.getUser(session).IsInGroup(collab)) {
            %>
            <a href="#" onclick="leaveCollab('<%= collab.GetName() + "','" + collab.GetID()%>'); return false;">Leave collab</a>
            <%
            } else if (collab.IsPublic()) {
            %>
            <a href="#" onclick="joinCollab('<%= collab.GetID()%>'); return false;">Join collab</a>
            <%
            } else {
            %>
            <a href="#" onclick="error('Function not implemented yet'); return false;">Ask for invitation</a>
            <%
                    }
                }
            %>
        </div>
    </div>
</div>