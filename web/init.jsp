<%--
    Document   : init
    Created on : 13.01.2019, 00:13:15
    Author     : J
--%>

<%@page import="snippet.Navigation"%>


<%
    class Main {

        private HttpSession session;

        public Main(HttpSession session) {
            this.session = session;
        }

        public void InitMenu() {
            Navigation.NavList nl;
            nl = new Navigation.NavList();

            // Navigation Snippet
            Navigation.NavObject nOSnippet = new Navigation.NavObject();

            nOSnippet.setName(
                    "Snippet");
            nOSnippet.setLink(
                    "browsesnippets");
            nl.addObject(nOSnippet);

            // Navigation Browse Snippets
            Navigation.NavObject nOSnippetBrowse = new Navigation.NavObject(session);

            nOSnippetBrowse.setName(
                    "Browse");
            nOSnippetBrowse.setLink(
                    "browsesnippets");
            nOSnippet.appendNavigation(nOSnippetBrowse);

            // Navigation My Snippets
            Navigation.NavObject nOSnippetMy = new Navigation.NavObject(session);

            nOSnippetMy.setName(
                    "My Snippets");
            nOSnippetMy.setOnlyLoggedIn(
                    true);
            nOSnippetMy.setLink(
                    "mysnippets");
            nOSnippet.appendNavigation(nOSnippetMy);

            // Navigation Collabs
            Navigation.NavObject nOCollab = new Navigation.NavObject(session);

            nOCollab.setName(
                    "Collabs");
            nOCollab.setLink(
                    "browsecollabs");
            nl.addObject(nOCollab);

            // Navigation Browse Collabs
            Navigation.NavObject nOCollabBrowse = new Navigation.NavObject(session);

            nOCollabBrowse.setName(
                    "Browse");
            nOCollabBrowse.setLink(
                    "browsecollabs");
            nOCollab.appendNavigation(nOCollabBrowse);

            // Navigation My Collabs
            Navigation.NavObject nOCollabMy = new Navigation.NavObject(session);

            nOCollabMy.setName(
                    "My Collabs");
            nOCollabMy.setOnlyLoggedIn(
                    true);
            nOCollabMy.setLink(
                    "collabs");
            nOCollab.appendNavigation(nOCollabMy);

            // Navigation Login
            Navigation.NavObject nOLogin = new Navigation.NavObject(session);

            nOLogin.setName(
                    "Login");
            nOLogin.setIntern(
                    false);
            nOLogin.setOnlyLoggedOut(
                    true);
            nOLogin.setLink(
                    "login.jsp");
            nl.addObject(nOLogin);

            // Navigation Login
            Navigation.NavObject nOSignUp = new Navigation.NavObject(session);

            nOSignUp.setName(
                    "Sign up");
            nOSignUp.setIntern(
                    false);
            nOSignUp.setOnlyLoggedOut(
                    true);
            nOSignUp.setLink(
                    "register.jsp");
            nl.addObject(nOSignUp);

            // Navigation Account
            Navigation.NavObject nOAccount = new Navigation.NavObject(session);

            nOAccount.setName(
                    "Account");
            nOAccount.setOnlyLoggedIn(
                    true);
            nOAccount.setLink(
                    "profile");
            nl.addObject(nOAccount);

            // Navigation Profile
            Navigation.NavObject nOAccountProfile = new Navigation.NavObject(session);

            nOAccountProfile.setName(
                    "Profile");
            nOAccountProfile.setOnlyLoggedIn(
                    true);
            nOAccountProfile.setLink(
                    "profile");
            nOAccount.appendNavigation(nOAccountProfile);

            // Navigation Logout
            Navigation.NavObject nOAccountLogout = new Navigation.NavObject(session);

            nOAccountLogout.setName(
                    "Logout");
            nOAccountLogout.setIntern(
                    false);
            nOAccountLogout.setOnlyLoggedIn(
                    true);
            nOAccountLogout.setLink(
                    "logout.jsp");
            nOAccount.appendNavigation(nOAccountLogout);

            // Navigation New Snippet
            Navigation.NavObject nONewSnippet = new Navigation.NavObject(session);

            nONewSnippet.setName(
                    "New snippet");
            nONewSnippet.setLink(
                    "writesnippet");
            nONewSnippet.setClasses(
                    "new-snippet");
            nl.addObject(nONewSnippet);

            Navigation.setNavigation(nl);
        }
    }
%>
<%
    Main main = new Main(session);
    main.InitMenu();
%>