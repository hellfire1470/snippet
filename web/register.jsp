<%--
    Document   : register
    Created on : 12.02.2019, 13:17:27
    Author     : Alexander
--%>
<%@page import="snippet.Authi.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String register = request.getParameter("register");
    String password1 = request.getParameter("password1");
    String password2 = request.getParameter("password2");
    String username = request.getParameter("username");
    String email = request.getParameter("email");
    String firstname = request.getParameter("firstname");
    String lastname = request.getParameter("lastname");

    if (register != null && password1 != null && password2 != null && username != null
            && email != null && firstname != null && lastname != null) {
        if (password1.equals(password2)) {
            RegistrationResult rr = Auth.Register(username, password1, email, firstname, lastname);
            if (rr == RegistrationResult.SUCCESS) {
                response.sendRedirect("index.jsp");
            }
        }
    }
%>

<html lang="DE">
    <!-- Meta -->
    <meta charset="utf-8">
    <title>Snippet</title>
    <meta name="description" content="Snippet Frontend">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/basic.css">
    <link rel="stylesheet" href="css/menus.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>

    <link href="css/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/pnotify.custom.min.js"></script>

    <script src="js/basic.js"></script>
    <!-- Shims -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Web App -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">

</head>
<body>

    <div id="wrapper">

        <div class="inside">

            <div id="content" class="container login-container">

                <div class="box centered">

                    <div class="logo">
                        <h1><a href="index.jsp">snippet</a></h1>
                        <p>your code database.</p>
                    </div>
                    <form method="post" id="register" autocomplete="off">
                        <div class="widget text-widget">
                            <input type="email" class="text email" name="email" placeholder="Your E-Mail address" />
                        </div>
                        <div class="widget text-widget">
                            <input type="text" class="text" name="username" placeholder="Username" />
                        </div>
                        <div class="widget text-widget">
                            <input type="password" class="text password" name="password1" placeholder="Password" />
                        </div>
                        <div class="widget text-widget">
                            <input type="password" class="text password" name="password2" placeholder="Confirm Password" />
                        </div>
                        <div class="widget text-widget">
                            <input type="text" class="text" name="firstname" placeholder="First name" />
                        </div>
                        <div class="widget text-widget">
                            <input type="text" class="text" name="lastname" placeholder="Last name" />
                        </div>
                        <div class="widget submit-widget">
                            <input type="submit" class="btn green submit" name="register" value="Register" />
                        </div>
                    </form>
                    <nav>
                        <ul>
                            <li><a href="login.jsp">Login</a></li>
                            <li><a href="help.jsp">Help</a></li>
                        </ul>
                    </nav>

                </div>

            </div>

        </div>

    </div>
    <script>
        $('#register [name=register]').on("click", () => {
            let form = $('#register');
            let email = form.find('[name=email]').val();
            let pass1 = form.find('[name=password1]').val();
            let pass2 = form.find('[name=password2]').val();
            let user = form.find('[name=username]').val();
            let first = form.find('[name=firstname]').val();
            let last = form.find('[name=lastname]').val();

            if (!email || !pass1 || !pass2 || !user || !first || !last) {
                error("Fülle alle Felder aus!", 5000);
                return false;
            }

            if (pass1 !== pass2) {
                error("Die Passwörter stimmen nicht überein!", 5000);
                return false;
            }

            if (pass1.length < 8) {
                error("Das Passwort muss mindestens 8 Zeichen lang sein", 5000);
                return false;
            }
            if (pass1.length > 32) {
                error("Das Passwort darf maximal 32 Zeichen lang sein", 5000);
                return false;
            }

            if (user.length < 4) {
                error("Der Benutzername muss mindestens 4 Zeichen lang sein", 5000);
                return false;
            }
            if (user.length > 32) {
                error("Der Benutzername darf maximal 32 Zeichen lang sein", 5000);
                return false;
            }
            return true;
        });
    </script>
</body>
</html>
