<%--
    Document   : Help
    Created on : 16.02.2019, 20:06:08
    Author     : J
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="DE">
    <!-- Meta -->
    <meta charset="utf-8">
    <title>Snippet</title>
    <meta name="description" content="Snippet Frontend">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/basic.css">
    <link rel="stylesheet" href="css/menus.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- Shims -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Web App -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">

</head>
<body>

    <div id="wrapper">

        <div class="inside">

            <div id="content" class="container login-container">

                <div class="box centered">
                    <div class="logo">
                        <h1><a href="index.jsp">snippet</a></h1>
                        <p>your code database.</p>
                    </div>
                    <div class="faq">
                        <section>
                            <h3>What is this?</h3>
                            <p>This is snippet, a web-based code database, based on PHP and MySQL.</p>
                        </section>
                        <section>
                            <h3>How much does it cost?</h3>
                            <p>Nothing! For you, snippet is completely free. You can sign in <a href="login.jsp">here</a> if you already have an account or register if you click <a href="register.jsp">here</a>.</p>
                        </section>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="login.jsp">Login</a></li>
                            <li><a href="register.jsp">Register</a></li>
                        </ul>
                    </nav>
                </div>

            </div>

        </div>

    </div>

</body>
</html>