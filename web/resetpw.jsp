<%--
    Document   : resetqpw
    Created on : 16.02.2019, 20:19:02
    Author     : J
--%>
<%@page import="snippet.Authi.Auth"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String success = (String) request.getParameter("success");
    String error = (String) request.getParameter("error");
    String resetpw = (String) request.getParameter("resetpw");

    if (resetpw != null && resetpw == "true") {

    }
%>
<!DOCTYPE html>
<html lang="DE">
    <!-- Meta -->
    <meta charset="utf-8">
    <title>Snippet</title>
    <meta name="description" content="Snippet Frontend">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/basic.css">
    <link rel="stylesheet" href="css/menus.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- Shims -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Web App -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">

</head>
<body>

    <div id="wrapper">

        <div class="inside">

            <div id="content" class="container login-container">

                <div class="box centered">

                    <div class="logo">
                        <h1><a href="index.php">snippet</a></h1>
                        <p>your code database.</p>
                    </div>
                    <form id="login" autocomplete="off" method="post">
                        <%
                            if (success != null && success.equals("true")) {
                        %>
                        Your Password hwas changed, please check your emails.
                        <%
                        } else {
                        %><%= error%>
                        }
                        %>
                        <div class="widget text-widget">
                            <input type="text" class="text email" name="email" placeholder="Email" />
                        </div>
                        <div class="widget submit-widget">
                            <input type="submit" class="btn green submit" name="resetpw" value="Retrieve a new Password" />
                        </div>
                    </form>
                    <nav>
                        <ul>
                            <li><a href="login.jsp">Login</a></li>
                            <li><a href="register.jsp">Register</a></li>
                            <li><a href="help.jsp">Help</a></li>
                        </ul>
                    </nav>

                </div>
            </div>

        </div>

    </div>

</body>
</html>
