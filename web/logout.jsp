<%--
    Document   : logout
    Created on : 12.02.2019, 18:19:11
    Author     : J
--%>

<%@page import="snippet.Authi.*" %>
<%
    Auth.logout(session, response);
%>
<html lang="DE">
    <!-- Meta -->
    <meta charset="utf-8">
    <title>Snippet</title>
    <meta name="description" content="Snippet Frontend">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/basic.css">
    <link rel="stylesheet" href="css/menus.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- Shims -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Web App -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">

</head>
<body>

    <div id="wrapper">

        <div class="inside">

            <div id="content" class="container login-container">

                <div class="box centered">

                    <div class="logo">
                        <h1><a href="index.jsp">snippet</a></h1>
                        <p>your code database.</p>
                    </div>
                    <section>
                        <p>You have been successfully logged out.</p>
                        <p>We already miss you! You can go back and login yourself in by clicking <a href="index.jsp">here</a>.</p>
                    </section>
                    <nav>
                        <ul>
                            <li><a href="login.jsp">Login</a></li>
                            <li><a href="help.jsp">Help</a></li>
                        </ul>
                    </nav>


                </div>
            </div>

        </div>

    </div>

</body>
</html>