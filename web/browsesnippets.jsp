<%--
    Document   : browsesnippets
    Created on : 12.02.2019, 16:42:59
    Author     : J
--%>

<%@include file="functions/snippet.jsp" %>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="snippet.Authi.DBCONN"%>
<!-- Snippet List !-->
<h1>All snippets</h1>
<div class="snippetList" id="sl-recent">
    <%
        Connection conn = DBCONN.getConnection();

        String sql = "SELECT "
                + "snippet.snippetid AS ID "
                + "FROM snippet "
                + "JOIN snippetmeta ON snippet.snippetid = snippetmeta.owner "
                + //"JOIN shares ON snippet.snippetid = shares.snippetid ".
                "WHERE (snippetmeta.metakey = 'ISPUBLIC' AND snippetmeta.metavalue = 1) "
                + "ORDER BY snippet.created DESC "
                + "LIMIT 0,20";

        List<Integer> snippetIds = new ArrayList<Integer>();

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()) {
            snippetIds.add(resultSet.getInt("ID"));
        }
        resultSet.close();
        statement.close();

        for (Integer snippetId : snippetIds) {
    %>

    <jsp:include page="functions/viewSnippet.jsp">
        <jsp:param name="snippet" value="<%= snippetId%>"></jsp:param>
    </jsp:include>
    <%
        }

    %>
</div>
