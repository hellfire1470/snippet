<%--
    Document   : snippet
    Created on : 18.02.2019, 16:12:23
    Author     : J
--%>
<%@page import="snippet.lib.strtotime"%>
<%@page import="snippet.Authi.Auth"%>
<%@page import="snippet.lib.SnippetUser"%>
<%@page import="snippet.lib.Snippet"%>
<%@page import="snippet.lib.Language"%>
<%@page import="snippet.lib.Tag"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if (!Auth.isLoggedIn(session)) {
        return;
    }
    int id = 0;
    if (request.getParameter("id") != null) {
        id = Integer.parseInt(request.getParameter("id"));
    }

    Snippet snippet = Snippet.GetByID(Snippet.class, id);
    if (!snippet.isLoaded()) {
        return;
    }
    SnippetUser su = SnippetUser.Cast(SnippetUser.class, Auth.getUser(session));
    if (!su.HasSnippet(snippet)) {
        return;
    }
    //snippet.GetLanguage().GetName()
%>
<h1>Edit your snippet</h1>
<div id="mysnippet" class="snippetSingle editSnippet">
    <h3><span contentEditable="false" spellcheck="false" id="mysnippet_name" class="editable"><%= snippet.GetAttr("title")%></span></h3>

    created on <span><%= strtotime.strtotime(snippet.GetAttr("created"))%></span>
    last edited on <span><% if (snippet.GetAttr("created") != snippet.GetAttr("lastedit")) {%> <%= snippet.GetAttr("created")%><% } else { %>-<% }%></span>
    <div class="snippetCode">
        <textarea id="edit_snippet_code" name="edit_snippet_code" style="display:none;"><%= snippet.GetAttr("code")%></textarea></div>
</div>

<!-- LANGUAGE -->

<div class="snippetSettings edit-setting-bar">
    <h4>Language</h4>
    <span id="snippet_language_current"><%= snippet.GetLanguage().GetName()%></span>
    <select id="snippet_language_new" style="display: none;">
        <%
            //
            for (Language lang : Language.GetLanguages()) {
                if (snippet.GetLanguage().GetID() == lang.GetID()) {
        %>
        <option value="<%= lang.GetID()%>" selected><%= lang.GetName()%></option>
        <%
        } else {
        %>
        <option value="<%= lang.GetID()%>"><%= lang.GetName()%></option>
        <%
                }
            }
        %>
    </select>

    <!-- PUBLIC -->
    <fieldset class="checkbox_container">
        <span>
            <input type="checkbox" name="ispublic" id="chk_snippet_public" disabled <% if (snippet.HasMeta("ISPUBLIC")) {
                    if (snippet.GetMeta("ISPUBLIC").equals("1")) { %>checked<% }
                        }%>/>
            <label for="chk_snippet_public"> public</label>
        </span>
    </fieldset>
</div>
<div class="snippetTags edit-setting-bar">
    <!-- TAGS -->
    <h4>Tags:</h4>
    <ul>
        <%
            for (Tag tag : snippet.GetTags()) {
        %>
        <li><%= tag.GetAttr("tag")%></li>
            <%
                }
            %>
    </ul>
    <!-- Buttons -->
</div>
<div class="edit-setting-bar snippetEditButtons">
    <button id="btn_snippet_edit" class="btn green submit">Edit snippet</button>
    <button id="btn_snippet_save" style="display: none;" class="btn green submit">Save changes</button>
    <button id="btn_snippet_delete" class="btn green submit">Delete snippet</button>
</div>

<!-- Error container -->
<div id="mysnippet_error"></div>

<!-- Scripts -->
<link rel="stylesheet" href="js/codemirror/lib/codemirror.css">
<script src="js/codemirror/lib/codemirror.js"></script>
<script src="js/codemirror/mode/javascript/javascript.js"></script>
<script>
    var editor = CodeMirror.fromTextArea($('#edit_snippet_code')[0], {
        lineNumbers: true,
        mode: "javascript",
        readOnly: true
    });
    $('#btn_snippet_edit').click(function () {
        $(this).hide();
        $('#btn_snippet_save').show();
        $('#mysnippet_name').attr("contentEditable", "true");
        $('#mysnippet_name').addClass("mysnippet_editable");
        editor.options.readOnly = false;
        editor.refresh();
        $('#snippet_language_current').hide();
        $('#snippet_language_new').show();
        $('#chk_snippet_public').attr('disabled', false);
    });
    $('#btn_snippet_save').click(function () {
        // clear error
        $('#mysnippet_error').html('');
        var name = $('#mysnippet_name').html();
        var code = editor.getValue();
        var language = $('#snippet_language_new').val();
        var ispublic = $('#chk_snippet_public').prop("checked");
        var tags = [];
        updateSnippet("<%= snippet.GetID()%>", name, code, language, ispublic, tags, function () {
            $('#btn_snippet_save').hide();
            $('#btn_snippet_edit').show();
            $('#mysnippet_name').attr("contentEditable", "false");
            $('#mysnippet_name').removeClass("mysnippet_editable");
            editor.options.readOnly = true;
            editor.refresh();
            $('#snippet_language_current').html($('#snippet_language_new :selected').html());
            $('#snippet_language_new').hide();
            $('#snippet_language_current').show();
            $('#chk_snippet_public').attr('disabled', true);
            success('Snippet updated!');
        }, function (err) {
            error(err);
        });
    });
    $('#btn_snippet_delete').click(function () {
        if (confirm('Are you sure you want to delete this snippet?')) {
            removeSnippet(<%= snippet.GetID()%>);
        }
    });

    var updateSnippet = function (id, name, code, language, ispublic, tags, success, error) {
        $.ajax({
            url: "Snippet",
            type: "POST",
            dataType: "json",
            data: {action: "update", id: id, name: name, code: code, language: language, ispublic: ispublic, tags: tags}
        }).done(function (result) {
            if (result.success == 1) {
                success();
            } else {

                error(result.error);
            }
        });
    }

    var removeSnippet = function (id) {
        $.ajax({
            url: "Snippet",
            type: "POST",
            dataType: "json",
            data: {action: "remove", id: id}
        }).done(function (result) {
            if (result.success == 1) {
                location.href = "?nav=mysnippets&view=overview";
            } else {
                error(result.error);
            }
        });
    }
</script>