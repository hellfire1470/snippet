<%--
    Document   : overview
    Created on : 18.02.2019, 15:59:19
    Author     : Alexander
--%>

<%@page import="snippet.lib.Snippet"%>
<%@page import="snippet.lib.SnippetUser"%>
<%@page import="snippet.Authi.Auth"%>
<%
    if (!Auth.isLoggedIn(session)) {
        return;
    }

    SnippetUser mysnippetuser = SnippetUser.Cast(SnippetUser.class, Auth.getUser(session));

%>
<h1>My snippets</h1>

<div id="mysnippets">
    <%        for (Snippet snippet : mysnippetuser.GetSnippets()) {%>
    <jsp:include page="/functions/viewSnippet.jsp">
        <jsp:param name="snippet" value="<%= snippet.GetID()%>"></jsp:param>
    </jsp:include>
    <% }%>
</div>

<script>
    $('.mysnippet').each(function (index) {
        if (index === 0) {
            return;
        }
        var id = $(this).find('.mysnippet_id').html();
        $(this).find('.mysnippet_public input').click(function () {
            var ispublic = $(this).prop('checked');
            updateSnippetShare(id, ispublic, function () { }, function (error) {
                alert(error);
            });
        });
    });
</script>
