<%--
    Document   : collabs
    Created on : 18.02.2019, 20:58:08
    Author     : Alexander
--%>
<%@page import="snippet.lib.Collab"%>
<%@page import="snippet.Authi.Auth"%>
<%@page import="snippet.Authi.Group"%>
<%@page import="snippet.Authi.User"%>
<%
    String view = request.getParameter("view");
    if (view != null && view.trim().length() > 0) {
        if (view.equals("users")) {
%>
<%@include file="collabs/viewusers.jsp" %>
<% return;
} else if (view.equals("snippets")) {%>
<%@include file="collabs/viewsnippets.jsp" %>
<%  return;
} else if (view.equals("profile")) {%>
<%@include file="collabs/collabprofile.jsp" %>
<% return;
} else if (view.equals("settings")) {%>
<%@include file="collabs/collabsettings.jsp" %>
<% return;
        }
    }

    String submit = request.getParameter("submit");
    String action = request.getParameter("action");

    if (submit != null && submit.trim().length() > 0
            && action != null && action.trim().length() > 0) {

        if (action.equals("au")) {

            String email = request.getParameter("email");
            String group = request.getParameter("group");
            User user = User.GetByTag(User.class, "email", email);
            Group c = Group.GetByID(Group.class, Integer.parseInt(group));
            if (Auth.getUser(session).IsInGroup(c)) {
                c.AddUser(user);
            }
        } else if (action.equals("cg")) {

            String name = request.getParameter("name");
            String description = request.getParameter("description");
            if (name != null && name.trim().length() > 0
                    && description != null && description.trim().length() > 0) {
                Group c = new Group();
                c.SetAttr("name", name);
                c.SetAttr("description", description);
                if (c.create()) {
                    User u = Auth.getUser(session);
                    c.AddUser(u);
                    c.SetUserMeta(u, "ADMIN", "1");
                    c.SetUserMeta(u, "WRITESNIPPETS", "1");
                    c.SetUserMeta(u, "VIEWUSERS", "1");
                    c.SetUserMeta(u, "VIEWSNIPPETS", "1");
                }
            }
        }
    }
%>

<div class="page-content">
    <h1>Your collabs</h1>
    <div class="collab-area">
        <div class="collab-list">
            <%
                for (Group group : Auth.getUser(session).GetGroups()) {
            %>
            <jsp:include page="/collabs/viewcollab.jsp">
                <jsp:param name="collab" value="<%= group.GetID()%>"></jsp:param>
            </jsp:include>
            <%
                    } %>
        </div>
    </div>
    <%
        if (!Auth.isLoggedIn(session)) {
            return;
        }
    %>
    <h1>create new collab</h1>
    <div class="page-section new-collab-section">
        <form method="post">
            <input type="hidden" name="action" value="cg">
            <input class="text" type="text" placeholder="name" name="name">
            <input class="text" type="text" placeholder="description" name="description">
            <input type="submit" class="btn green submit" name="submit" value="Create">
        </form>
    </div>
</div>
